﻿using UnityEngine;
using System.Collections;

public class Cupula : MonoBehaviour
{

	public  Transform center;
	public  Vector3 axis = Vector3.up;
	public  float radius;
	public  float radiusSpeed;
	public  float rotationSpeed;
	public  int RANDOM_FACTOR;

	gameControl controller;
	boxDealer dealer;


	public void Awake ()
	{
		controller = GameObject.FindGameObjectWithTag ("GameController").GetComponent<gameControl> ();
//		dealer = GameObject.FindGameObjectWithTag ("BoxDealer").GetComponent<boxDealer> ();

	}

	void Start ()
	{
		center = transform;
	}

	void Update ()
	{
		int rn = Random.Range (0, RANDOM_FACTOR);
		int rn2 = Random.Range (0, RANDOM_FACTOR);

		//ROTATE AROUND GLOBO
		foreach (Transform te in transform.GetChild(0).transform) {
			
			Vector3 r = new Vector3 (Random.Range (rn, rn2), Random.Range (rn, rn2), Random.Range (rn, rn2));
			var desiredPosition = (te.position - center.position).normalized * radius + te.position;

			//ROTATE AROUND GLOBO
			te.RotateAround (transform.position, r, rotationSpeed * Time.deltaTime);
			//SET RADIO
			te.position = Vector3.MoveTowards (te.position, desiredPosition, Time.deltaTime * radiusSpeed);
			//ROTATE ITSELF
			te.Rotate (r, Time.deltaTime * Random.Range (rn, rn2));
//			}
		}
	}

	public void releaseObjects ()
	{
		
		foreach (Transform te in transform.GetChild(0).transform) {
			te.GetComponent<Box> ().SetBackToPiedra ();

		}
	}

	public void Purge ()
	{		
		dealer = GameObject.FindGameObjectWithTag ("BoxDealer").GetComponent<boxDealer> ();
		
		foreach (Transform te in transform.GetChild(0).transform) {
			int level = te.GetComponent<Box> ().levelBox;

			if ((level) <= (controller.level - 5)) {
				dealer.RemoveFromGameBoxes (te.gameObject);
				Destroy (te.gameObject);
				dealer.CleanSort ();
				print ("PURGA");
			}
		}


	}

}
