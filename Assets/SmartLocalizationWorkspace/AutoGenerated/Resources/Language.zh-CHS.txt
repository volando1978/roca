<?xml version="1.0" encoding="utf-8"?>
<root><!-- 
    Microsoft ResX Schema 
    
    Version 2.0
    
    The primary goals of this format is to allow a simple XML format 
    that is mostly human readable. The generation and parsing of the 
    various data types are done through the TypeConverter classes 
    associated with the data types.
    
    Example:
    
    ... ado.net/XML headers & schema ...
    <resheader name="resmimetype">text/microsoft-resx</resheader>
    <resheader name="version">2.0</resheader>
    <resheader name="reader">System.Resources.ResXResourceReader, System.Windows.Forms, ...</resheader>
    <resheader name="writer">System.Resources.ResXResourceWriter, System.Windows.Forms, ...</resheader>
    <data name="Name1"><value>this is my long string</value><comment>this is a comment</comment></data>
    <data name="Color1" type="System.Drawing.Color, System.Drawing">Blue</data>
    <data name="Bitmap1" mimetype="application/x-microsoft.net.object.binary.base64">
        <value>[base64 mime encoded serialized .NET Framework object]</value>
    </data>
    <data name="Icon1" type="System.Drawing.Icon, System.Drawing" mimetype="application/x-microsoft.net.object.bytearray.base64">
        <value>[base64 mime encoded string representing a byte array form of the .NET Framework object]</value>
        <comment>This is a comment</comment>
    </data>
                
    There are any number of "resheader" rows that contain simple 
    name/value pairs.
    
    Each data row contains a name, and value. The row also contains a 
    type or mimetype. Type corresponds to a .NET class that support 
    text/value conversion through the TypeConverter architecture. 
    Classes that don't support this are serialized and stored with the 
    mimetype set.
    
    The mimetype is used for serialized objects, and tells the 
    ResXResourceReader how to depersist the object. This is currently not 
    extensible. For a given mimetype the value must be set accordingly:
    
    Note - application/x-microsoft.net.object.binary.base64 is the format 
    that the ResXResourceWriter will generate, however the reader can 
    read any of the formats listed below.
    
    mimetype: application/x-microsoft.net.object.binary.base64
    value   : The object must be serialized with 
            : System.Runtime.Serialization.Formatters.Binary.BinaryFormatter
            : and then encoded with base64 encoding.
    
    mimetype: application/x-microsoft.net.object.soap.base64
    value   : The object must be serialized with 
            : System.Runtime.Serialization.Formatters.Soap.SoapFormatter
            : and then encoded with base64 encoding.

    mimetype: application/x-microsoft.net.object.bytearray.base64
    value   : The object must be serialized into a byte array 
            : using a System.ComponentModel.TypeConverter
            : and then encoded with base64 encoding.
    -->
  <xsd:schema id="root" xmlns="" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:msdata="urn:schemas-microsoft-com:xml-msdata">
    <xsd:import namespace="http://www.w3.org/XML/1998/namespace" />
    <xsd:element name="root" msdata:IsDataSet="true">
      <xsd:complexType>
        <xsd:choice maxOccurs="unbounded">
          <xsd:element name="metadata">
            <xsd:complexType>
              <xsd:sequence>
                <xsd:element name="value" type="xsd:string" minOccurs="0" />
              </xsd:sequence>
              <xsd:attribute name="name" use="required" type="xsd:string" />
              <xsd:attribute name="type" type="xsd:string" />
              <xsd:attribute name="mimetype" type="xsd:string" />
              <xsd:attribute ref="xml:space" />
            </xsd:complexType>
          </xsd:element>
          <xsd:element name="assembly">
            <xsd:complexType>
              <xsd:attribute name="alias" type="xsd:string" />
              <xsd:attribute name="name" type="xsd:string" />
            </xsd:complexType>
          </xsd:element>
          <xsd:element name="data">
            <xsd:complexType>
              <xsd:sequence>
                <xsd:element name="value" type="xsd:string" minOccurs="0" msdata:Ordinal="1" />
                <xsd:element name="comment" type="xsd:string" minOccurs="0" msdata:Ordinal="2" />
              </xsd:sequence>
              <xsd:attribute name="name" type="xsd:string" use="required" msdata:Ordinal="1" />
              <xsd:attribute name="type" type="xsd:string" msdata:Ordinal="3" />
              <xsd:attribute name="mimetype" type="xsd:string" msdata:Ordinal="4" />
              <xsd:attribute ref="xml:space" />
            </xsd:complexType>
          </xsd:element>
          <xsd:element name="resheader">
            <xsd:complexType>
              <xsd:sequence>
                <xsd:element name="value" type="xsd:string" minOccurs="0" msdata:Ordinal="1" />
              </xsd:sequence>
              <xsd:attribute name="name" type="xsd:string" use="required" />
            </xsd:complexType>
          </xsd:element>
        </xsd:choice>
      </xsd:complexType>
    </xsd:element>
  </xsd:schema>
  <resheader name="resmimetype">
    <value>text/microsoft-resx</value>
  </resheader>
  <resheader name="version">
    <value>2.0</value>
  </resheader>
  <resheader name="reader">
    <value>System.Resources.ResXResourceReader, System.Windows.Forms, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089</value>
  </resheader>
  <resheader name="writer">
    <value>System.Resources.ResXResourceWriter, System.Windows.Forms, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089</value>
  </resheader>
	<data name="acelero" xml:space="preserve">
		<value>傾斜</value>
	</data>
	<data name="agate" xml:space="preserve">
		<value>瑪瑙</value>
	</data>
	<data name="amethist" xml:space="preserve">
		<value>紫晶</value>
	</data>
	<data name="areyousure" xml:space="preserve">
		<value>你確定?</value>
	</data>
	<data name="best" xml:space="preserve">
		<value>最好</value>
	</data>
	<data name="buttons" xml:space="preserve">
		<value>鈕扣</value>
	</data>
	<data name="congrats" xml:space="preserve">
		<value>祝賀</value>
	</data>
	<data name="continue" xml:space="preserve">
		<value>繼續?</value>
	</data>
	<data name="controls" xml:space="preserve">
		<value>控制</value>
	</data>
	<data name="crystal" xml:space="preserve">
		<value>水晶</value>
	</data>
	<data name="diamond" xml:space="preserve">
		<value>鑽石</value>
	</data>
	<data name="endgame" xml:space="preserve">
		<value>完</value>
	</data>
	<data name="entering" xml:space="preserve">
		<value>進入一個新的領域</value>
	</data>
	<data name="gameover" xml:space="preserve">
		<value>遊戲結束</value>
	</data>
	<data name="howto" xml:space="preserve">
		<value>怎麼玩</value>
	</data>
	<data name="ice" xml:space="preserve">
		<value>冰</value>
	</data>
	<data name="leaderboard" xml:space="preserve">
		<value>板</value>
	</data>
	<data name="meters" xml:space="preserve">
		<value>米</value>
	</data>
	<data name="music" xml:space="preserve">
		<value>音樂</value>
	</data>
	<data name="next" xml:space="preserve">
		<value>下一個</value>
	</data>
	<data name="night" xml:space="preserve">
		<value>在晚上！</value>
	</data>
	<data name="nodelete" xml:space="preserve">
		<value>不！回去</value>
	</data>
	<data name="ok" xml:space="preserve">
		<value>好！</value>
	</data>
	<data name="ok2" xml:space="preserve">
		<value>好！</value>
	</data>
	<data name="onemoretime" xml:space="preserve">
		<value>多一個</value>
	</data>
	<data name="opal" xml:space="preserve">
		<value>蛋白石</value>
	</data>
	<data name="play" xml:space="preserve">
		<value>玩</value>
	</data>
	<data name="potion" xml:space="preserve">
		<value>劑量</value>
	</data>
	<data name="push" xml:space="preserve">
		<value>推</value>
	</data>
	<data name="reached" xml:space="preserve">
		<value>到達</value>
	</data>
	<data name="record" xml:space="preserve">
		<value>記錄！</value>
	</data>
	<data name="reset" xml:space="preserve">
		<value>復位</value>
	</data>
	<data name="resume" xml:space="preserve">
		<value>回去</value>
	</data>
	<data name="rubi" xml:space="preserve">
		<value>魯維</value>
	</data>
	<data name="saphire" xml:space="preserve">
		<value>藍寶石</value>
	</data>
	<data name="settings" xml:space="preserve">
		<value>設置</value>
	</data>
	<data name="sound" xml:space="preserve">
		<value>聲音</value>
	</data>
	<data name="speed" xml:space="preserve">
		<value>速度</value>
	</data>
	<data name="touch" xml:space="preserve">
		<value>屏幕觸摸</value>
	</data>
	<data name="turning" xml:space="preserve">
		<value>車削</value>
	</data>
	<data name="tutorial" xml:space="preserve">
		<value>點擊向左或向右瓜分。
握住水龍頭瓜分更難。
鬆開來提高速度。
通過弧線傳遞將釋放克隆一起玩。
不要讓黑暗抓住你！
祝你好運！</value>
	</data>
	<data name="watch" xml:space="preserve">
		<value>**觀看視頻和...</value>
	</data>
	<data name="water" xml:space="preserve">
		<value>水</value>
	</data>
	<data name="world" xml:space="preserve">
		<value>世界</value>
	</data>
	<data name="yesdelete" xml:space="preserve">
		<value>是的，它刪除</value>
	</data>
</root>