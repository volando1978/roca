﻿using UnityEngine;
using System.Collections;

public class FacesHud : MonoBehaviour
{

	gameControl controller;


	void Awake ()
	{
		controller = GameObject.FindGameObjectWithTag ("GameController").GetComponent<gameControl> ();
	}


	public void showFaces ()
	{

		clearFaces ();
		StartCoroutine ("showTheFaces");

	}


	IEnumerator showTheFaces ()
	{

		for (int i = 0; i < controller.numBoxes; i++) {
			//pinta todo
//			gameObject.transform.GetChild (i).gameObject.SetActive (true);
			yield return new WaitForSeconds (0.1f);
		}


		for (int i = 0; i < controller.gatheredBoxes; i++) {
			
			// para cada player left, se activa un dot
//			gameObject.transform.GetChild (i).transform.GetChild (0).gameObject.SetActive (true);
			yield return new WaitForSeconds (0.1f);
		}


		yield return new WaitForSeconds (0);
	}

	void clearFaces ()
	{


		for (int i = 0; i < gameObject.transform.childCount; i++) {
			gameObject.transform.GetChild (i).gameObject.SetActive (false);

		}
	}
}
