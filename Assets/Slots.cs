﻿using UnityEngine;
using System.Collections;

using System.Collections.Generic;


public class Slots : MonoBehaviour
{

	public  GameObject slot1, slot2, slot3, TallySprites, BoxGo;
	//	public GameObject ObjectTally;


	public GameObject HudGo;
	//	public GameObject boxDealer;
	BingoText hud;
	gameControl controller;
	Vector3[] poses;
	GameObject[] lista;
	public int premio;


	void Start ()
	{
		poses = new Vector3[3];
		poses [0] = slot1.GetComponent<RectTransform> ().position;
		poses [1] = slot2.GetComponent<RectTransform> ().position;
		poses [2] = slot3.GetComponent<RectTransform> ().position;


		lista = new GameObject[3];

		hud = HudGo.GetComponent<BingoText> ();
		controller = GameObject.FindGameObjectWithTag ("GameController").GetComponent<gameControl> ();



	}

	public void Mete (GameObject g)
	{
		GameObject ga = Instantiate (g, transform.position, Quaternion.identity) as GameObject;



		Destroy (lista [2]);
		lista [2] = lista [1];
		lista [1] = lista [0];
		lista [0] = ga;


		SetPosInList ();



		CheckCombination ();
	}

	void SetPosInList ()
	{
		poses [0] = slot1.GetComponent<RectTransform> ().position;
		poses [1] = slot2.GetComponent<RectTransform> ().position;
		poses [2] = slot3.GetComponent<RectTransform> ().position;

		for (int i = 0; i <= 2; i++) {
			if (lista [i] != null) {
				lista [i].transform.position = poses [i];
				lista [i].transform.localScale = slot1.transform.localScale;
//				print ("POSES: " + lista [i].transform.position + " " + poses [i]);
				ReduceObject (lista [i]);
			}
		}


	}

	void CheckCombination ()
	{
		if (lista [0] != null && lista [1] != null && lista [2] != null) {
			for (int i = 0; i < lista.Length; i++) {
				
				int a = lista [0].GetComponent<Box> ().levelBox;
				int b = lista [1].GetComponent<Box> ().levelBox;
				int c = lista [2].GetComponent<Box> ().levelBox;

//			int uno, dos, tres, cuatro;
//			uno = a + b + c;
//			dos = a - b + c;
//			tres = a + b - c;
//			cuatro = a - b - c;


				if (a == b && b == c) {
					premio += 3;
//					print ("C1: a == b == c" + premio);

				} else if (a == b && b != c) {
					premio += 2;
//					print ("C2: a == b != c" + premio);

				}
//			else if (a != b == c) {
//				premio += 1;
//				print ("C3: a != b == c" + premio);
//
//			} else if (a != b != c) {
//				premio -= 3;
//				print ("C4: a != b != c" + premio);
//
//			}


			}
		} 
		hud.DisplayBingoText ();
		controller.UpdateHUD ();
	}

	//	public void ClearTally ()
	//	{
	//		if (ObjectTally) {
	//			Destroy (ObjectTally);
	//		}
	//	}

	//	public void StartTally ()
	//	{
	//
	//
	////		ObjectTally = Instantiate (TallySprites, transform.position, Quaternion.identity) as GameObject;
	////		ObjectTally.name = "TALLY";
	////		ObjectTally.tag = "Tally";
	////		ObjectTally.transform.localScale = new Vector3 (0.1f, 0.1f, 0.1f);
	//
	//
	////		ObjectTally.transform.parent = transform;
	//		print ("TALLY CREATED");
	//	}

	public void SetTally ()
	{
		TallySprites.GetComponent<Tally> ().setTally ();
		hud.DisplayBingoText ();

	}

	void ReduceObject (GameObject g)
	{
		g.GetComponentInChildren<MeshRenderer> ().enabled = true;
//		g.GetComponentInChildren<MeshRenderer> ().material.color = Color.blue;
		g.GetComponent<Rigidbody> ().isKinematic = true;
		g.GetComponent<Box> ().enabled = false;
		g.GetComponent<BoxCollider> ().enabled = false;
		print ("TALLY POS " + g.transform.position);
	}

	void Update ()
	{
//		SetPosInList ();
//		if (ObjectTally) {
//			ObjectTally.transform.position = tally.GetComponent<RectTransform> ().position;
//		}
//		if (transform.GetChild (0).gameObject != null)
//		print ("TALLY POS " + ObjectTally.transform.position);

	}
}
