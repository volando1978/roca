﻿using UnityEngine;
using System.Collections;

public class Controles : MonoBehaviour
{

	gameControl gc;

	public int ClampRestriction;
	public int ClampRestrictionY;

	Vector3 direction = Vector3.one;
	Quaternion rotation = Quaternion.identity;


	Vector2 touchOrigin;


	void Start ()
	{
		gc = GetComponent<gameControl> ();
	}

	void Translate (float x, float y)
	{
		var perpendicular = new Vector3 (-direction.y, direction.x);
		var verticalRotation = Quaternion.AngleAxis (y * Time.deltaTime, perpendicular);
		var horizontalRotation = Quaternion.AngleAxis (x * Time.deltaTime, direction);
		rotation *= horizontalRotation * verticalRotation;
	}



	void Update ()
	{

		if (gameControl.currentState == gameControl.State.INGAME) {

			#if UNITY_EDITOR || UNITY_STANDALONE_OSX || UNITY_STANDALONE_WIN

			if (gc.currentGlobo != null) {


				if (Input.GetAxis ("Vertical") < 0) {

					gc.currentGlobo.transform.RotateAround (Vector3.zero, Camera.main.transform.TransformDirection (Vector3.left), gc.speedYGlobo * Time.deltaTime);
					gc.cameraTarget.transform.RotateAround (Vector3.zero, Camera.main.transform.TransformDirection (Vector3.left), gc.speedYGlobo * Time.deltaTime);

//					Translate (gc.speedXGlobo, 0);
//					Translate (gc.speedXGlobo, 0);

//					gc.currentGlobo.transform.Translate (0, 0, -gc.speedYGlobo * Time.deltaTime, Space.Self);
//					gc.cameraTarget.transform.Translate (0, 0, -gc.speedYGlobo * Time.deltaTime, Space.Self);
//				
//					Vector3 clampedPosition = gc.currentGlobo.transform.position;
//					clampedPosition.z = Mathf.Clamp (gc.currentGlobo.transform.position.z, -ClampRestrictionY, ClampRestrictionY);
//					gc.cameraTarget.transform.position = gc.currentGlobo.transform.position = clampedPosition;
				}
				if (Input.GetAxis ("Vertical") > 0) {

					gc.currentGlobo.transform.RotateAround (Vector3.zero, Camera.main.transform.TransformDirection (Vector3.right), gc.speedYGlobo * Time.deltaTime);
					gc.cameraTarget.transform.RotateAround (Vector3.zero, Camera.main.transform.TransformDirection (Vector3.right), gc.speedYGlobo * Time.deltaTime);
					print ("arriba");

//					gc.currentGlobo.transform.Translate (0, 0, gc.speedYGlobo * Time.deltaTime, Space.Self);
//					gc.cameraTarget.transform.Translate (0, 0, gc.speedYGlobo * Time.deltaTime, Space.Self);
//
//					Vector3 clampedPosition = gc.currentGlobo.transform.position;
//					clampedPosition.z = Mathf.Clamp (gc.currentGlobo.transform.position.z, -ClampRestrictionY, ClampRestrictionY);
//					gc.cameraTarget.transform.position = gc.currentGlobo.transform.position = clampedPosition;
				}

				if (Input.GetAxis ("Horizontal") < 0) {
					
//					gc.currentGlobo.transform.RotateAround (Vector3.zero, Vector3.up, gc.speedXGlobo * Time.deltaTime);
//					gc.cameraTarget.transform.RotateAround (Vector3.zero, Vector3.up, gc.speedXGlobo * Time.deltaTime);
					gc.cameraTarget.transform.Rotate (Vector3.up * gc.speedXGlobo * Time.deltaTime);
					gc.currentGlobo.transform.Rotate (Vector3.up * gc.speedXGlobo * Time.deltaTime);




//					gc.cameraTarget.transform.Rotate (0, gc.speedXGlobo * 2 * Time.deltaTime, 0);
//					gc.currentGlobo.transform.Rotate (0, gc.speedXGlobo * 2 * Time.deltaTime, 0, Space.World);
//
//					Quaternion clampedRotation = gc.currentGlobo.transform.localRotation;
//					clampedRotation.y = Mathf.Clamp (gc.cameraTarget.transform.localRotation.y, -ClampRestriction, ClampRestriction);
//					gc.cameraTarget.transform.localRotation = gc.currentGlobo.transform.localRotation = clampedRotation;

//					gc.currentGlobo.transform.Translate (-gc.speedYGlobo * Time.deltaTime, 0, 0, Space.Self);
//					gc.cameraTarget.transform.Translate (-gc.speedYGlobo * Time.deltaTime, 0, 0, Space.Self);
//
//					Vector3 clampedPosition = gc.currentGlobo.transform.position;
//					clampedPosition.z = Mathf.Clamp (gc.currentGlobo.transform.position.z, -ClampRestrictionY, ClampRestrictionY);
//					gc.cameraTarget.transform.position = gc.currentGlobo.transform.position = clampedPosition;


				} else if (Input.GetAxis ("Horizontal") > 0) {

//					gc.currentGlobo.transform.RotateAround (Vector3.zero, Vector3.down, gc.speedXGlobo * Time.deltaTime);
//					gc.cameraTarget.transform.RotateAround (Vector3.zero, Vector3.down, gc.speedXGlobo * Time.deltaTime);
					gc.cameraTarget.transform.Rotate (Vector3.down * gc.speedXGlobo * Time.deltaTime);
					gc.currentGlobo.transform.Rotate (Vector3.down * gc.speedXGlobo * Time.deltaTime);


//					gc.cameraTarget.transform.Rotate (0, -gc.speedXGlobo * 2 * Time.deltaTime, 0);
//					gc.currentGlobo.transform.Rotate (0, -gc.speedXGlobo * 2 * Time.deltaTime, 0, Space.World);
//
//					Quaternion clampedRotation = gc.currentGlobo.transform.localRotation;
//					clampedRotation.y = Mathf.Clamp (gc.cameraTarget.transform.localRotation.y, -ClampRestriction, ClampRestriction);
//					gc.cameraTarget.transform.localRotation = gc.currentGlobo.transform.localRotation = clampedRotation;

//					gc.currentGlobo.transform.Translate (gc.speedYGlobo * Time.deltaTime, 0, 0, Space.Self);
//					gc.cameraTarget.transform.Translate (gc.speedYGlobo * Time.deltaTime, 0, 0, Space.Self);
//
//					Vector3 clampedPosition = gc.currentGlobo.transform.position;
//					clampedPosition.z = Mathf.Clamp (gc.currentGlobo.transform.position.z, -ClampRestrictionY, ClampRestrictionY);
//					gc.cameraTarget.transform.position = gc.currentGlobo.transform.position = clampedPosition;
				}

			} 


			if (Input.GetKey (KeyCode.Space)) {
				gc.toGame ();
			}


			#endif

			#if UNITY_IOS || UNITY_ANDROID// && !UNITY_EDITOR

			if (gameControl.currentState == gameControl.State.INGAME) {



				Vector2 touchPos = Vector2.zero;

				if (Input.touchCount > 1) {

					Vector3 clampedPosition;
					Quaternion clampedRotation;

					gc.currentGlobo.transform.Translate (0, 0, ((gc.speedYGlobo)) * Time.deltaTime);
					gc.cameraTarget.transform.Rotate (0, 0, 0);
					gc.currentGlobo.transform.Rotate (0, 0, 0);
					gc.terrain.transform.Rotate (0, 0, 0);

					clampedRotation = gc.currentGlobo.transform.localRotation;
					clampedRotation.y = Mathf.Clamp (gc.cameraTarget.transform.localRotation.y, -60, 60);
					gc.cameraTarget.transform.localRotation = gc.currentGlobo.transform.localRotation = clampedRotation;

					clampedPosition = gc.currentGlobo.transform.position;
					clampedPosition.z = Mathf.Clamp (gc.currentGlobo.transform.position.z, -ClampRestrictionY, ClampRestrictionY);
					gc.cameraTarget.transform.position = gc.currentGlobo.transform.position = clampedPosition;
					return;
				}

				if (Input.touchCount > 0 && Input.GetTouch (0).phase == TouchPhase.Began) {
					touchOrigin = Input.GetTouch (0).position;
				}

				if (Input.touchCount > 0 && Input.GetTouch (0).phase == TouchPhase.Moved) {
					touchPos = Input.GetTouch (0).position;
					Move (touchPos);
				}


				if (Input.touchCount > 0 && Input.GetTouch (0).phase == TouchPhase.Stationary) {
					touchPos = Input.GetTouch (0).position;
					Move (touchPos);
				}
			}
			#endif
		}
	}

	void Move (Vector2 touchPos)
	{
		if (gc.currentGlobo != null) {//&& Input.mousePosition.y < Screen.height / 8

			Vector3 clampedPosition;
			Quaternion clampedRotation;

			float acc = Mathf.Abs ((Screen.width / 2) - touchPos.x) / 60;

			if (touchPos.x < Screen.width - Screen.width / 3 && touchPos.x > Screen.width / 3) {
				gc.currentGlobo.transform.Translate (0, 0, ((gc.speedYGlobo)) * Time.deltaTime);

				clampedPosition = gc.currentGlobo.transform.position;
				clampedPosition.z = Mathf.Clamp (gc.currentGlobo.transform.position.z, -ClampRestrictionY, ClampRestrictionY);
				gc.cameraTarget.transform.position = gc.currentGlobo.transform.position = clampedPosition;
				return;
			}

			if (touchPos.x > Screen.width / 3) {
				gc.cameraTarget.transform.Rotate (0, (gc.speedXGlobo + acc) * Time.deltaTime, 0);
				gc.currentGlobo.transform.Rotate (0, (gc.speedXGlobo + acc) * Time.deltaTime, 0);
				gc.terrain.transform.Rotate (0, (gc.speedXGlobo + acc) * Time.deltaTime, 0);

				clampedRotation = gc.currentGlobo.transform.localRotation;
				clampedRotation.y = Mathf.Clamp (gc.cameraTarget.transform.localRotation.y, -60, 60);
				gc.cameraTarget.transform.localRotation = gc.currentGlobo.transform.localRotation = clampedRotation;
			}

			if (touchPos.x < Screen.width / 3) {
				gc.cameraTarget.transform.Rotate (0, -(gc.speedXGlobo + acc) * Time.deltaTime, 0);
				gc.currentGlobo.transform.Rotate (0, -(gc.speedXGlobo + acc) * Time.deltaTime, 0);
				gc.terrain.transform.Rotate (0, -(gc.speedXGlobo + acc) * Time.deltaTime, 0);

				clampedRotation = gc.currentGlobo.transform.localRotation;
				clampedRotation.y = Mathf.Clamp (gc.cameraTarget.transform.localRotation.y, -60, 60);
				gc.cameraTarget.transform.localRotation = gc.currentGlobo.transform.localRotation = clampedRotation;
			}

		}
	}
}
