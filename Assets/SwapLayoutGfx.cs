﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class SwapLayoutGfx : MonoBehaviour
{

	Transform i1;

	public float spd;

	gameControl controller;


	void Awake ()
	{
		controller = GameObject.FindGameObjectWithTag ("GameController").GetComponent<gameControl> ();
		i1 = transform.GetChild (0).transform;

	}


	public void ThrowCurtain ()
	{
		StartCoroutine ("Lift");
	}

	public void RemoveCurtain ()
	{
		StartCoroutine ("Unlift");
	}


	IEnumerator Lift ()
	{
		StopCoroutine ("Unlift");
		float start = i1.position.y;
		float startTime = Time.time;
		float overTime = 1.5f;


		while (i1.position.y < 0) {
			float posy = Mathf.Lerp (start, -1, (Time.time - startTime) / overTime);
			Vector3 pos = new Vector3 (i1.transform.position.x, posy, i1.transform.position.z);
			i1.transform.position = pos;

			yield return null;

			if (i1.position.y <= 0) {
				StopCoroutine ("Lift");
			}
		}


		yield return null;
	}

	IEnumerator Unlift ()
	{
		StopCoroutine ("Lift");
		float start = i1.position.y;
		float startTime = Time.time;
		float overTime = 0.5f;


		while (i1.position.y > -500) {
			float posy = Mathf.Lerp (start, -500, (Time.time - startTime) / overTime);
			Vector3 pos = new Vector3 (i1.transform.position.x, posy, i1.transform.position.z);
			i1.transform.position = pos;

			yield return null;

			if (i1.position.y <= -500) {
				StopCoroutine ("Unlift");
			}
		}

		yield return null;
	}

}
