﻿using UnityEngine;
using System.Collections;

public class Transicion : MonoBehaviour
{

	private Texture2D texture;
	int resolution = 8;
	public Color color;


	public void Start ()
	{
		
	}

	private void Awake ()
	{
		texture = new Texture2D (resolution, resolution, TextureFormat.RGB24, false);
//		texture.filterMode = FilterMode.Point;
//		texture.name = "Procedural Texture";
//		GetComponent<MeshRenderer> ().material.mainTexture = texture;
	}



	public void FillCam ()
	{
		
		StartCoroutine ("Wipe");
	}


	IEnumerator Wipe ()
	{

		for (int i = 0; i < 8; i++) {
			for (int j = 0; j < 8; j++) {
				
				texture.SetPixel (i, j, color);//new Color (10, 10, 255, 255)

//				print (i);
				texture.Apply ();

				yield return new WaitForSeconds (0.0001f);
			}
		}
	}

}
//
//	Camera cam1;
//	Camera cam2;
//	float wipeTime = 2.0;
//	floatrotateAmount = 360.0;
//	Mesh shapeMesh;
//	AnimationCurve curve;
//	private bool inProgress = false;
//	private bool swap = false;
//	private Material shapeMaterial;
//	private Transform shape;
//
//	function Update () {
//		if (Input.GetKeyDown("up")) {
//			DoWipe();
//		}
//		else if (Input.GetKeyDown("down")) {
//			DoWipe();
//		}
//
//	}
//
//	IEnumerator DoWipe () {
//		if (inProgress) return;
//		inProgress = true;
//
//		swap = !swap;
//		yield ScreenWipe.use.ShapeWipe (swap? camera1 : camera2, swap? camera2 : camera1, wipeTime, zoom, shapeMesh[useShape], rotateAmount);
//		//yield ScreenWipe.use.ShapeWipe (swap? camera1 : camera2, swap? camera2 : camera1, wipeTime, zoom, shapeMesh[useShape], rotateAmount, curve);
//
//		inProgress = false;
//	}
//
//	void  ShapeWipe (Camera cam1, Camera cam2, float time, Mesh mesh) {
//
//		if (!shapeMaterial) {
//			shapeMaterial = new Material (
//				"Shader \"DepthMask\" {" +
//				"   SubShader {" +
//				"	   Tags { \"Queue\" = \"Background\" }" +
//				"	   Lighting Off ZTest LEqual ZWrite On Cull Off ColorMask 0" +
//				"	   Pass {}" +
//				"   }" +
//				"}"
//			);
//		}
//		if (!shape) {
//			shape = new GameObject("Shape", MeshFilter, MeshRenderer).transform;
//			shape.GetComponent<Renderer>().material = shapeMaterial;
//		}
//
//		CameraSetup (cam1, cam2, true, false);
//		var useCam = (zoom == ZoomType.Shrink)? cam1 : cam2;
//		var otherCam = (zoom == ZoomType.Shrink)? cam2 : cam1;
//		var originalDepth = otherCam.depth;
//		var originalClearFlags = otherCam.clearFlags;
//		otherCam.depth = useCam.depth+1;
//		otherCam.clearFlags = CameraClearFlags.Depth;
//
//		shape.gameObject.active = true;
//		(shape.GetComponent(MeshFilter) as MeshFilter).mesh = mesh;
//		shape.position = otherCam.transform.position + otherCam.transform.forward * (otherCam.nearClipPlane+.01);
//		shape.parent = otherCam.transform;
//		shape.localRotation = Quaternion.identity;
//
//		if( useCurve ){
//			var rate = 1.0/time;
//			if (zoom == ZoomType.Shrink) {
//				for (i = 1.0; i > 0.0; i -= Time.deltaTime * rate) {
//					var t = curve.Evaluate( i );
//					shape.localScale = Vector3(t, t, t);
//					shape.localEulerAngles = Vector3(0.0, 0.0, i * rotateAmount);
//					yield;
//				}
//			}
//			else {
//				for (i = 0.0; i < 1.0; i += Time.deltaTime * rate) {
//					t = curve.Evaluate( i );
//					shape.localScale = Vector3(t, t, t);
//					shape.localEulerAngles = Vector3(0.0, 0.0, -i * rotateAmount);
//					yield;
//				}
//			}
//		}
//		else{
//			rate = 1.0/time;
//			if (zoom == ZoomType.Shrink) {
//				for (i = 1.0; i > 0.0; i -= Time.deltaTime * rate) {
//					t = Mathf.Lerp(1.0, 0.0, Mathf.Sin((1.0-i) * Mathf.PI * 0.5));	// Slow down near the end
//					shape.localScale = Vector3(t, t, t);
//					shape.localEulerAngles = Vector3(0.0, 0.0, i * rotateAmount);
//					yield;
//				}
//			}
	
//			else {
//				for (i = 0.0; i < 1.0; i += Time.deltaTime * rate) {
//					t = Mathf.Lerp(1.0, 0.0, Mathf.Sin((1.0-i) * Mathf.PI * 0.5));		// Start out slower
//					shape.localScale = Vector3(t, t, t);
//					shape.localEulerAngles = Vector3(0.0, 0.0, -i * rotateAmount);
//					yield;
//				}
//			}
//		}
//
//		otherCam.clearFlags = originalClearFlags;
//		otherCam.depth = originalDepth;
//		CameraCleanup (cam1, cam2);
//		shape.parent = null;
//		shape.gameObject.active = false;
//	}
		
