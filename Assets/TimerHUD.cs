﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class TimerHUD : MonoBehaviour
{

	float message;
	gameControl controller;
	bool running = false;


	public void Awake ()
	{
		controller = GameObject.FindGameObjectWithTag ("GameController").GetComponent<gameControl> ();
	}

	public void DisplayTimer ()
	{
		message = controller.timer;
		GetComponent<Text> ().text = "0:" + message.ToString ("F0");
	}
}
