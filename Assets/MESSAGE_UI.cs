﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

using SmartLocalization;

public class MESSAGE_UI : MonoBehaviour
{


	LanguageManager localizator;
	string message;
	gameControl controller;

	bool running = false;


	public void Awake ()
	{

		LanguageManager localizator = LanguageManager.Instance;
//		message = localizator.GetTextValue ("entering").ToString ();

		controller = GameObject.FindGameObjectWithTag ("GameController").GetComponent<gameControl> ();
	}


	public  void DisplayBoxesLeft ()
	{
		message = controller.gatheredBoxes.ToString () + " / " + controller.remainingBoxes.ToString ();
		GetComponent<Text> ().text = message;
	}


}
