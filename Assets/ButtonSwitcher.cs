﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ButtonSwitcher : MonoBehaviour
{


	gameControl controller;
	public Sprite Up;
	public Sprite Down;

	// Use this for initialization
	void Awake ()
	{
	
		controller = GameObject.FindGameObjectWithTag ("GameController").GetComponent<gameControl> ();

	}

	public void changeImage (bool isMovingMountain)
	{
		if (isMovingMountain) {
			GetComponent<Image> ().sprite = Up;
		} else {
			GetComponent<Image> ().sprite = Down;
		}



	}
	

}
