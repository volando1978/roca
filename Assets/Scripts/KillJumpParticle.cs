﻿using UnityEngine;
using System.Collections;

public class KillJumpParticle : MonoBehaviour
{

		int t = 14;
		AudioSource audio;

		// Use this for initialization
		IEnumerator Start ()
		{
				
				audio = GetComponent<AudioSource> ();
				audio.mute = globales.sfxSwitch;
				audio.pitch = 0.5f;
				audio.Play ();

				while (t > 0) {
						t--;
						yield return new WaitForSeconds (0);
				}

				Destroy (gameObject);

		}

}
