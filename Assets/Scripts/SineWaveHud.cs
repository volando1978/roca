﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class SineWaveHud : MonoBehaviour
{
	[Range (0, 3)]
	public float
		t = 0;
	[Range (0, 30)]
	public float
		dt = .08f;
	[Range (0, 30)]
	public float
		amplitude = 1f;
	[Range (0, 30)]
	public float
		frequency = 0.3f;
	public int
		diameter;
	[Range (-30, 3)]
	public int
		offsetX;
	public int population;

	LineRenderer lr;

	
	List <GameObject> balls = new List<GameObject> ();
	public GameObject ballPrefab;
	
	
	// Use this for initialization
	void Awake ()
	{
		frequency = Random.Range (22, 23);
//				population = 28;
		diameter = (Screen.width / population) + 1;
//
//		lr = GetComponent<LineRenderer> ();
//		lr.SetVertexCount (population);

		for (int i = 0; i < population; i++) {
			GameObject ball = Instantiate (ballPrefab);
			ball.transform.parent = transform;
			balls.Add (ball);

//			lr.SetPosition (i, RectTransformUtility.ScreenPointToWorldPointInRectangle (ball.transform.position, Camera.main, ball.transform.position, Camera.main.WorldToScreenPoint (ball.transform.position)));
		}



	}


	void Update ()
	{
		for (int i = 0; i < balls.Count; i++) {
			Vector3 pos = (Vector3)balls [i].transform.position;
			Vector3 offset = new Vector3 (Screen.width / 2, 0, 0);
			Vector3 sine = new Vector3 (i * diameter, (amplitude * Mathf.Sin (frequency * (t + i))), pos.z);

			balls [i].transform.position = sine + transform.position - offset;
			balls [i].transform.localScale = Vector3.one;
			t += dt; 
//			lr.SetPosition (i, balls [i].transform.position);

		}

		Vector3 dir = Vector3.zero;
		dir.y = Input.acceleration.y;

		
//		print ("dir + " + dir.y);
		
		if (dir.y < -0.13f) {
			frequency = Mathf.Lerp (frequency, 23, Time.deltaTime);
		} 
		
		if (dir.y > 0.13f) {
			frequency = Mathf.Lerp (frequency, 22, Time.deltaTime);
		}

		dir.x = Input.acceleration.x;
		
		
//				print ("dir + " + dir.x);

		if (dir.x < -0.13f) {
			amplitude = Mathf.Lerp (amplitude, 23, Time.deltaTime);
		} 
		
		if (dir.x > 0.13f) {
			amplitude = Mathf.Lerp (amplitude, 22, Time.deltaTime);
		} 
		
	}
}
