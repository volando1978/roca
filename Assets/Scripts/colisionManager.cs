﻿using UnityEngine;
using System.Collections;


//manages collisions of player
public class colisionManager : MonoBehaviour
{

	public GameObject newPlayer;
	public GameObject particleDeath;

	public GameObject gameController;




	void Start ()
	{
		globales.dead = false;
		gameController = GameObject.FindGameObjectWithTag ("GameController");

	}

	void LateUpdate ()
	{

		//MUERTE POR CAIDA
		transform.position = transform.parent.transform.position;
		if (transform.position.y < -5) {
//			if (!globales.checkPlayersMissing ()) {
//				globales.playerFelt = true;
//				globales.dead = true;
//				globales.coordContinue = gameObject.transform.parent.transform.position;
////								print ("pilla Coor caida " + globales.coordContinue);
//				Destroy (gameObject.transform.parent.gameObject);
//
//			} else {
////				gameController.GetComponent<gameControl> ().selectPlayer ();
//				globales.coordContinue = gameObject.transform.parent.transform.position;
//				Destroy (gameObject.transform.parent.gameObject);
//
//			}
		}
	}

	void OnTriggerEnter (Collider col)
	{

		if (col.gameObject.tag == "Rock" || col.gameObject.tag == "Bar") {

			if (globales.collisionsOn) {
				Instantiate (particleDeath, transform.position, Quaternion.identity);

				if (!globales.checkPlayersMissing ()) {
					SoundManager.playCrashClip ();
//										print ("DEAD");
					globales.dead = true;
					globales.coordContinue = gameObject.transform.parent.transform.position;
					print ("pilla Coor collision roca " + globales.coordContinue);

					Destroy (gameObject.transform.parent.gameObject);

				} else {
					SoundManager.playSmallCrashClip ();
//										gameController.GetComponent<gameControl> ().selectPlayer ();
					globales.coordContinue = gameObject.transform.parent.transform.position;

					Destroy (gameObject.transform.parent.gameObject);

				}
			}
		}

		if (col.gameObject.tag == "cupula" || col.gameObject.tag == "cupulaVecina") {

			if (globales.collisionsOn) {

				col.gameObject.transform.GetChild (0).transform.Translate (Vector3.one * Time.deltaTime * 60, Space.World);


				if (col.gameObject.transform.GetChild (0).transform.localPosition.y < 2f) {
					SoundManager.playCheckOutClip ();
				}

				if (col.gameObject.transform.GetChild (0).transform.localPosition.y < 3f) {

//										for (int i =0; i< ProgController.NMinions; i++) {
					Instantiate (newPlayer, transform.position, Quaternion.identity);
//										}
				}

			}
		}
	}
}


