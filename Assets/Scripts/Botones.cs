﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.Advertisements;
using SmartLocalization;

using System;

public class Botones : MonoBehaviour
{

	LanguageManager localizator;

	void setLanguage ()
	{
//				localizator = LanguageManager.Instance;

		switch (Application.systemLanguage) {

		case SystemLanguage.Catalan:
			localizator.ChangeLanguage ("ca");
			break;
		case SystemLanguage.ChineseSimplified:
			localizator.ChangeLanguage ("zh-CHT");
			break;
		case SystemLanguage.ChineseTraditional:
			localizator.ChangeLanguage ("zh-CHS");
			break;
		case SystemLanguage.English:
			localizator.ChangeLanguage ("en");
			break;
		case SystemLanguage.French:
			localizator.ChangeLanguage ("fr");
			break;
		case SystemLanguage.German:
			localizator.ChangeLanguage ("de");
			break;
		case SystemLanguage.Italian:
			localizator.ChangeLanguage ("it");
			break;
		case SystemLanguage.Japanese:
			localizator.ChangeLanguage ("ja");
			break;
		case SystemLanguage.Portuguese:
			localizator.ChangeLanguage ("pt");
			break;
		case SystemLanguage.Russian:
			localizator.ChangeLanguage ("ru");
			break;
		case SystemLanguage.Spanish:
			localizator.ChangeLanguage ("es");
			break;
		default:
			localizator.ChangeLanguage ("en");
			break;						
				
		}
	}

	void Start ()
	{
		updateHUD ();
		localizator = LanguageManager.Instance;
		setLanguage ();
//		localizator.ChangeLanguage ("gl");

		print ("IDIOMA: " + Application.systemLanguage.ToString ());



		transform.GetChild (46).gameObject.transform.GetChild (0).GetComponent<Text> ().text = localizator.GetTextValue ("acelero"); // acelero
		transform.GetChild (50).gameObject.GetComponent<Text> ().text = localizator.GetTextValue ("areyousure"); // acelero
		transform.GetChild (17).gameObject.GetComponent<Text> ().text = localizator.GetTextValue ("best");// Best
		transform.GetChild (46).gameObject.transform.GetChild (1).GetComponent<Text> ().text = localizator.GetTextValue ("buttons"); //buttons
		transform.GetChild (35).gameObject.GetComponent<Text> ().text = localizator.GetTextValue ("congrats"); // acelero
		transform.GetChild (28).gameObject.transform.GetChild (0).GetComponent<Text> ().text = localizator.GetTextValue ("continue");// Continue
		transform.GetChild (46).gameObject.transform.GetChild (3).GetComponent<Text> ().text = localizator.GetTextValue ("controls"); //Controls
		transform.GetChild (27).gameObject.transform.GetChild (0).GetComponent<Text> ().text = localizator.GetTextValue ("endgame");// End Game
		transform.GetChild (0).gameObject.GetComponent<Text> ().text = localizator.GetTextValue ("gameover"); // Game over
		transform.GetChild (44).gameObject.transform.GetChild (0).GetComponent<Text> ().text = localizator.GetTextValue ("howto"); // Howto
		transform.GetChild (7).gameObject.transform.GetChild (0).GetComponent<Text> ().text = localizator.GetTextValue ("leaderboard"); //Leaderboard
		transform.GetChild (1).gameObject.GetComponent<Text> ().text = localizator.GetTextValue ("meters");// Meters
		transform.GetChild (12).gameObject.transform.GetChild (0).GetComponent<Text> ().text = localizator.GetTextValue ("music"); // Music
		transform.GetChild (42).gameObject.GetComponent<Text> ().text = localizator.GetTextValue ("next");// Next at
//		transform.GetChild (23).gameObject.GetComponent<Text> ().text = localizator.GetTextValue ("night");// Next at
		transform.GetChild (52).gameObject.transform.GetChild (0).GetComponent<Text> ().text = localizator.GetTextValue ("nodelete");// Next at
		transform.GetChild (6).gameObject.transform.GetChild (0).GetComponent<Text> ().text = localizator.GetTextValue ("ok");// Next at
		transform.GetChild (36).gameObject.transform.GetChild (0).GetComponent<Text> ().text = localizator.GetTextValue ("ok2");// Next at
		transform.GetChild (32).gameObject.transform.GetChild (0).GetComponent<Text> ().text = localizator.GetTextValue ("onemoretime");// One More Time
		transform.GetChild (3).gameObject.transform.GetChild (0).GetComponent<Text> ().text = localizator.GetTextValue ("play");// Play
		transform.GetChild (15).gameObject.GetComponent<Text> ().text = localizator.GetTextValue ("record");// Next at
		transform.GetChild (38).gameObject.transform.GetChild (0).GetComponent<Text> ().text = localizator.GetTextValue ("reset"); // Reset Game
		transform.GetChild (9).gameObject.transform.GetChild (0).GetComponent<Text> ().text = localizator.GetTextValue ("resume");// Next at
		transform.GetChild (10).gameObject.transform.GetChild (0).GetComponent<Text> ().text = localizator.GetTextValue ("settings");// Settings
		transform.GetChild (11).gameObject.transform.GetChild (0).GetComponent<Text> ().text = localizator.GetTextValue ("sound"); // Sound
		transform.GetChild (46).gameObject.transform.GetChild (2).GetComponent<Text> ().text = localizator.GetTextValue ("touch"); // touch
		transform.GetChild (45).gameObject.GetComponent<Text> ().text = localizator.GetTextValue ("tutorial");// Next at
		transform.GetChild (29).gameObject.GetComponent<Text> ().text = localizator.GetTextValue ("watch");// Watch video and...
		transform.GetChild (51).gameObject.transform.GetChild (0).GetComponent<Text> ().text = localizator.GetTextValue ("yesdelete");// Next at
//		transform.GetChild (24).gameObject.transform.GetChild (0).GetComponent<Text> ().text = localizator.GetTextValue ("world");// Next at

	}


	public void updateHUD ()
	{


		if (gameControl.currentState == gameControl.State.GAMEOVER) {

			transform.GetChild (0).gameObject.SetActive (false); //Game Over
			transform.GetChild (1).gameObject.SetActive (true);//distance
			transform.GetChild (2).gameObject.SetActive (false);//jetski splash
			transform.GetChild (3).gameObject.SetActive (false);//play button
			transform.GetChild (4).gameObject.SetActive (false);//hud
			transform.GetChild (5).gameObject.SetActive (false);//world button
			transform.GetChild (6).gameObject.SetActive (false);//select button
			transform.GetChild (7).gameObject.SetActive (false);//select button
			transform.GetChild (8).gameObject.SetActive (false);//pause button
			transform.GetChild (9).gameObject.SetActive (false);//resume button
			transform.GetChild (10).gameObject.SetActive (false);//settings button
			transform.GetChild (11).gameObject.SetActive (false);//music button
			transform.GetChild (12).gameObject.SetActive (false);//sound button
			transform.GetChild (13).gameObject.SetActive (false);//ads button
			transform.GetChild (14).gameObject.SetActive (false);//tooltip L button

			if (globales.showNewRecord) {
				transform.GetChild (15).gameObject.SetActive (true);//best run button
				transform.GetChild (17).gameObject.SetActive (false);//last record so far run button

			} else {
				transform.GetChild (15).gameObject.SetActive (false);//best run button
				transform.GetChild (17).gameObject.SetActive (true);//last record so far run button

			}
			transform.GetChild (16).gameObject.SetActive (false);//cortina button
			transform.GetChild (18).gameObject.SetActive (false);//tooltip R button button
			transform.GetChild (19).gameObject.SetActive (true);//falda
			transform.GetChild (20).gameObject.SetActive (false);//restore button button
			transform.GetChild (21).gameObject.SetActive (false);// OK! tutorial button
			transform.GetChild (22).gameObject.SetActive (false);// BOX 2

			transform.GetChild (23).gameObject.SetActive (false);//BOX

			transform.GetChild (24).gameObject.SetActive (false);//night night* text
			transform.GetChild (25).gameObject.SetActive (false);//FREE PLAY
			transform.GetChild (26).gameObject.SetActive (true);//LEFT BOXES
			transform.GetChild (27).gameObject.SetActive (false);//End Game

//						if (Advertisement.isReady () && globales.isContinue && !globales.gameNight) {
			transform.GetChild (28).gameObject.SetActive (false);//Continue
			transform.GetChild (29).gameObject.SetActive (false);//Watch video and
//						}
			transform.GetChild (30).gameObject.SetActive (false);//Earn credits
			transform.GetChild (31).gameObject.SetActive (false);//total distance
			transform.GetChild (32).gameObject.SetActive (true);//one more time
			transform.GetChild (33).gameObject.SetActive (true);//progress bar
			transform.GetChild (34).gameObject.SetActive (true);//level display
			transform.GetChild (35).gameObject.SetActive (false);//level up pop up
			transform.GetChild (36).gameObject.SetActive (false);//Button ok
			transform.GetChild (37).gameObject.SetActive (false);//Price image
			transform.GetChild (38).gameObject.SetActive (false);//Reset Game
			transform.GetChild (40).gameObject.SetActive (false);//stage meter bar
			transform.GetChild (41).gameObject.SetActive (false);//Reset Game
			transform.GetChild (42).gameObject.SetActive (false);//Remaining Meters
			transform.GetChild (43).gameObject.SetActive (true);//medal to get 
			transform.GetChild (44).gameObject.SetActive (false);//how to play 
			transform.GetChild (45).gameObject.SetActive (false);//tutorial
			transform.GetChild (46).gameObject.SetActive (false);// GAMEPAD MODE
			transform.GetChild (47).gameObject.SetActive (false);// BUTTON PUSH 
			transform.GetChild (48).gameObject.SetActive (false);// sine animation
			transform.GetChild (49).gameObject.SetActive (false);// sine animation
			transform.GetChild (50).gameObject.SetActive (false);// sine animation
			transform.GetChild (51).gameObject.SetActive (false);// sine animation
			transform.GetChild (52).gameObject.SetActive (false);// No, Go Back
			transform.GetChild (53).gameObject.SetActive (true);// Faces HUD

			
			
			
			

		} 

		if (gameControl.currentState == gameControl.State.CONTINUE) {
//						if (globales.isContinue) {
//								transform.GetChild (0).gameObject.SetActive (false); //Game Over
//						} else {
			transform.GetChild (0).gameObject.SetActive (true); //Game Over
				
//						}
			transform.GetChild (1).gameObject.SetActive (false);//distance
			transform.GetChild (2).gameObject.SetActive (false);//jetski splash
			transform.GetChild (3).gameObject.SetActive (false);//play button
			transform.GetChild (4).gameObject.SetActive (false);//hud
			transform.GetChild (5).gameObject.SetActive (false);//world button
			transform.GetChild (6).gameObject.SetActive (false);//select button
			transform.GetChild (7).gameObject.SetActive (false);//game center button
			transform.GetChild (8).gameObject.SetActive (false);//pause button
			transform.GetChild (9).gameObject.SetActive (false);//resume button
			transform.GetChild (10).gameObject.SetActive (false);//settings button
			transform.GetChild (11).gameObject.SetActive (false);//music button
			transform.GetChild (12).gameObject.SetActive (false);//sound button
			transform.GetChild (13).gameObject.SetActive (false);//ads button
			transform.GetChild (14).gameObject.SetActive (false);//tooltip L button
			
			if (globales.showNewRecord) {
				transform.GetChild (15).gameObject.SetActive (false);//best run button
				transform.GetChild (17).gameObject.SetActive (false);//last record so far run button
				
			} else {
				transform.GetChild (15).gameObject.SetActive (false);//best run button
				transform.GetChild (17).gameObject.SetActive (false);//last record so far run button
				
			}
			transform.GetChild (16).gameObject.SetActive (false);//cortina button
			transform.GetChild (18).gameObject.SetActive (false);//tooltip R button button
			transform.GetChild (19).gameObject.SetActive (true);//Falda
			transform.GetChild (20).gameObject.SetActive (false);//restore button button
			transform.GetChild (21).gameObject.SetActive (false);// OK! tutorial button
			transform.GetChild (22).gameObject.SetActive (false);// BOX 2
			
			if (globales.showNewRecordNight) {
				transform.GetChild (23).gameObject.SetActive (true);//BOX
				//								transform.GetChild (17).gameObject.SetActive (false);//last record so far run button
				
			} else {
				transform.GetChild (23).gameObject.SetActive (false);//BOX
				//								transform.GetChild (17).gameObject.SetActive (false);//last record so far run button
				
			}
			transform.GetChild (24).gameObject.SetActive (false);//night night* text
			transform.GetChild (25).gameObject.SetActive (false);//FREE PLAY
			transform.GetChild (26).gameObject.SetActive (false);//LEFT BOXES
			transform.GetChild (27).gameObject.SetActive (true);//End Game
			
			if (Advertisement.isReady () && globales.isContinue) {
				transform.GetChild (28).gameObject.SetActive (true);//Continue
				transform.GetChild (29).gameObject.SetActive (true);//Watch video and
			}
			transform.GetChild (30).gameObject.SetActive (false);//Earn credits
			transform.GetChild (31).gameObject.SetActive (false);//total distance
			transform.GetChild (32).gameObject.SetActive (false);//one more time
			transform.GetChild (33).gameObject.SetActive (false);//progress bar
			transform.GetChild (34).gameObject.SetActive (false);//level display
			transform.GetChild (35).gameObject.SetActive (false);//level up pop up
			transform.GetChild (36).gameObject.SetActive (false);//Button ok
			transform.GetChild (37).gameObject.SetActive (false);//Price image
			transform.GetChild (38).gameObject.SetActive (false);//Reset Game
			transform.GetChild (40).gameObject.SetActive (false);//stage meter bar
			transform.GetChild (41).gameObject.SetActive (false);//Reset Game
			transform.GetChild (42).gameObject.SetActive (false);//Remaining Meters
			transform.GetChild (43).gameObject.SetActive (false);//medal to get 
			transform.GetChild (44).gameObject.SetActive (false);//how to play 
			transform.GetChild (45).gameObject.SetActive (false);//tutorial
			transform.GetChild (46).gameObject.SetActive (false);// GAMEPAD MODE
			transform.GetChild (47).gameObject.SetActive (false);// BUTTON PUSH 
			transform.GetChild (48).gameObject.SetActive (false);// sine animation
			transform.GetChild (49).gameObject.SetActive (false);// sine animation
			transform.GetChild (50).gameObject.SetActive (false);// sine animation
			transform.GetChild (51).gameObject.SetActive (false);// sine animation
			transform.GetChild (52).gameObject.SetActive (false);// No, Go Back
			transform.GetChild (53).gameObject.SetActive (true);// Faces HUD

		} 

		if (gameControl.currentState == gameControl.State.MENU) {
			transform.GetChild (0).gameObject.SetActive (false);
			transform.GetChild (1).gameObject.SetActive (false);
			transform.GetChild (2).gameObject.SetActive (true);
			transform.GetChild (3).gameObject.SetActive (true); //PLAY BUTTON
			transform.GetChild (4).gameObject.SetActive (false);
			transform.GetChild (5).gameObject.SetActive (false);
			transform.GetChild (6).gameObject.SetActive (false);//back button
			transform.GetChild (7).gameObject.SetActive (true);//winners button
			transform.GetChild (8).gameObject.SetActive (false);//pause button
			transform.GetChild (9).gameObject.SetActive (false);//resume button
			transform.GetChild (10).gameObject.SetActive (true);//settings button
			transform.GetChild (11).gameObject.SetActive (false);//music button
			transform.GetChild (12).gameObject.SetActive (false);//sound button
			transform.GetChild (13).gameObject.SetActive (false);//ads button
			transform.GetChild (14).gameObject.SetActive (false);//ads button
			transform.GetChild (15).gameObject.SetActive (false);//best run button
			transform.GetChild (16).gameObject.SetActive (false);//ads button
			transform.GetChild (17).gameObject.SetActive (false);//last record so far run button
			transform.GetChild (18).gameObject.SetActive (false);//tooltip R button button
			transform.GetChild (19).gameObject.SetActive (true);//falda
			transform.GetChild (20).gameObject.SetActive (false);//restore button button
			transform.GetChild (21).gameObject.SetActive (false);// OK! tutorial button
			transform.GetChild (22).gameObject.SetActive (false);// BOX 2
			transform.GetChild (23).gameObject.SetActive (false);//BOX
			transform.GetChild (24).gameObject.SetActive (false);//night night* text
			transform.GetChild (25).gameObject.SetActive (false);//FREE PLAY
			transform.GetChild (26).gameObject.SetActive (false);//LEFT BOXES
			transform.GetChild (27).gameObject.SetActive (false);//End Game
			transform.GetChild (28).gameObject.SetActive (false);//Continue
			transform.GetChild (29).gameObject.SetActive (false);//Watch video and
			transform.GetChild (30).gameObject.SetActive (false);//Earn credits
			transform.GetChild (31).gameObject.SetActive (false);//total distance
			transform.GetChild (32).gameObject.SetActive (false);//one more time
			transform.GetChild (33).gameObject.SetActive (false);//progress bar
			transform.GetChild (34).gameObject.SetActive (false);//level display
			transform.GetChild (35).gameObject.SetActive (false);//level up pop up
			transform.GetChild (36).gameObject.SetActive (false);//Button ok
			transform.GetChild (37).gameObject.SetActive (false);//Price image
			transform.GetChild (38).gameObject.SetActive (false);//Reset Game
			transform.GetChild (40).gameObject.SetActive (false);//stage meter bar
			transform.GetChild (41).gameObject.SetActive (false);//Reset Game
			transform.GetChild (42).gameObject.SetActive (false);//Remaining Meters
			transform.GetChild (43).gameObject.SetActive (false);//medal to get 
			if (globales.numberOfGames < 10) {//if (isRightPressed && isLeftPressed) {
				transform.GetChild (44).gameObject.SetActive (false);//how to play 
			}
			transform.GetChild (45).gameObject.SetActive (false);//tutorial
			transform.GetChild (46).gameObject.SetActive (false);// GAMEPAD MODE
			transform.GetChild (47).gameObject.SetActive (false);// BUTTON PUSH 
			transform.GetChild (48).gameObject.SetActive (true);// sine animation
			transform.GetChild (49).gameObject.SetActive (false);// sine animation
			transform.GetChild (50).gameObject.SetActive (false);// sine animation
			transform.GetChild (51).gameObject.SetActive (false);// sine animation
			transform.GetChild (52).gameObject.SetActive (false);// No Go Back
			transform.GetChild (53).gameObject.SetActive (false);// Faces HUD

		}
			
		if (gameControl.currentState == gameControl.State.INGAME) {
			transform.GetChild (0).gameObject.SetActive (false);
			transform.GetChild (1).gameObject.SetActive (false);
			transform.GetChild (2).gameObject.SetActive (false);
			transform.GetChild (3).gameObject.SetActive (false);
			transform.GetChild (4).gameObject.SetActive (false);
			transform.GetChild (5).gameObject.SetActive (false);
			transform.GetChild (6).gameObject.SetActive (false);//select button
			transform.GetChild (7).gameObject.SetActive (false);//winners button
			transform.GetChild (8).gameObject.SetActive (true);//pause button
			transform.GetChild (9).gameObject.SetActive (false);//resume button
			transform.GetChild (10).gameObject.SetActive (false);//settings button
			transform.GetChild (11).gameObject.SetActive (false);//music button
			transform.GetChild (12).gameObject.SetActive (false);//sound button
			transform.GetChild (13).gameObject.SetActive (false);//ads button
			transform.GetChild (14).gameObject.SetActive (false);//BOX 1
			transform.GetChild (15).gameObject.SetActive (false);//best run button
			transform.GetChild (16).gameObject.SetActive (false);//ads button
			transform.GetChild (17).gameObject.SetActive (false);//last record so far run button
			#if UNITY_IOS || UNITY_ANDROID
			transform.GetChild (18).gameObject.SetActive (true);//switcher button ?
			#endif
			#if UNITY_EDITOR
			transform.GetChild (18).gameObject.SetActive (false);//switcher button ?
			#endif

			transform.GetChild (19).gameObject.SetActive (true);//falda
			transform.GetChild (20).gameObject.SetActive (false);//restore button button
			transform.GetChild (21).gameObject.SetActive (false);// OK! tutorial button
			transform.GetChild (22).gameObject.SetActive (false);// BOX 2
			transform.GetChild (23).gameObject.SetActive (false);//BOX
			transform.GetChild (24).gameObject.SetActive (true);//nStage Name text
			transform.GetChild (25).gameObject.SetActive (false);//FREE PLAY
			transform.GetChild (26).gameObject.SetActive (true);//LEFT BOXES
			transform.GetChild (27).gameObject.SetActive (false);//End Game
			transform.GetChild (28).gameObject.SetActive (false);//Continue
			transform.GetChild (29).gameObject.SetActive (false);//Watch video and
			transform.GetChild (30).gameObject.SetActive (false);//Earn LEFT BOXES
			transform.GetChild (31).gameObject.SetActive (false);//total distance
			transform.GetChild (32).gameObject.SetActive (false);//one more time
			transform.GetChild (33).gameObject.SetActive (false);//progress bar
			transform.GetChild (34).gameObject.SetActive (false);//level display
			transform.GetChild (35).gameObject.SetActive (false);//level up pop up
			transform.GetChild (36).gameObject.SetActive (false);//Button ok
			transform.GetChild (37).gameObject.SetActive (false);//Price image
			transform.GetChild (38).gameObject.SetActive (false);//Reset Game
			transform.GetChild (40).gameObject.SetActive (false);//stage meter bar
			transform.GetChild (41).gameObject.SetActive (false);//Reset Game
			transform.GetChild (42).gameObject.SetActive (false);//Remaining Meters
			transform.GetChild (43).gameObject.SetActive (false);//medal to get 
			transform.GetChild (44).gameObject.SetActive (false);//how to play 
			transform.GetChild (45).gameObject.SetActive (false);//tutorial
			transform.GetChild (46).gameObject.SetActive (false);// GAMEPAD MODE
			if (globales.controlsMode == 1) {
				transform.GetChild (47).gameObject.SetActive (false);// BUTTON PUSH 

			} else {
				transform.GetChild (47).gameObject.SetActive (false);// BUTTON PUSH 

			}
			transform.GetChild (48).gameObject.SetActive (true);// BINGO TEXT
			transform.GetChild (49).gameObject.SetActive (true);// TALLY
			transform.GetChild (50).gameObject.SetActive (false);// sine animation
			transform.GetChild (51).gameObject.SetActive (false);// sine animation
			transform.GetChild (52).gameObject.SetActive (false);// No, Go Back
			transform.GetChild (53).gameObject.SetActive (true);// Faces HUD


		}

		if (gameControl.currentState == gameControl.State.PAUSE) {
			transform.GetChild (0).gameObject.SetActive (false); //Game Over
			transform.GetChild (1).gameObject.SetActive (false);//distance
			transform.GetChild (2).gameObject.SetActive (false);//jetski splash
			transform.GetChild (3).gameObject.SetActive (false);//play button
			transform.GetChild (4).gameObject.SetActive (false);//hud
			transform.GetChild (5).gameObject.SetActive (false);//world button
			transform.GetChild (6).gameObject.SetActive (false);//select button
			transform.GetChild (7).gameObject.SetActive (false);//select button
			transform.GetChild (8).gameObject.SetActive (false);//select button
			transform.GetChild (9).gameObject.SetActive (true);//select button
			transform.GetChild (10).gameObject.SetActive (false);//settings button
			//AUDIO
			transform.GetChild (11).gameObject.SetActive (true);//music button
			if (globales.sfxSwitch) {
				transform.GetChild (11).gameObject.transform.GetChild (1).gameObject.SetActive (true);
			} else {
				transform.GetChild (11).gameObject.transform.GetChild (1).gameObject.SetActive (false);
				
			}
			
			transform.GetChild (12).gameObject.SetActive (true);//sound button
			if (globales.musicSwitch) {
				transform.GetChild (12).gameObject.transform.GetChild (1).gameObject.SetActive (true);
			} else {
				transform.GetChild (12).gameObject.transform.GetChild (1).gameObject.SetActive (false);
				
			}
			transform.GetChild (13).gameObject.SetActive (false);//ads button
			transform.GetChild (14).gameObject.SetActive (false);//BOX 1
			transform.GetChild (15).gameObject.SetActive (false);//best run button
			transform.GetChild (16).gameObject.SetActive (false);//ads button
			transform.GetChild (17).gameObject.SetActive (false);//last record so far run button
			transform.GetChild (18).gameObject.SetActive (false);//tooltip R button button
			if (!globales.goldenVersion) {
				transform.GetChild (19).gameObject.SetActive (true);//golden version button
			} else {
				transform.GetChild (19).gameObject.SetActive (true);//golden version button
				
			}
			//RESTORE
			#if UNITY_IOS
			if (!globales.goldenVersion) {
				transform.GetChild (20).gameObject.SetActive (false);//restore button button ios
			} else {
				transform.GetChild (20).gameObject.SetActive (false);//restore button button ios
			}
			#endif

			#if UNITY_ANDROID
			transform.GetChild (20).gameObject.SetActive (false);//restore button button android
			#endif

			transform.GetChild (21).gameObject.SetActive (false);// OK! tutorial button
			transform.GetChild (22).gameObject.SetActive (false);// BOX 2
			transform.GetChild (23).gameObject.SetActive (false);//BOX
			transform.GetChild (24).gameObject.SetActive (false);//stage name button
			transform.GetChild (25).gameObject.SetActive (false);//FREE PLAY
			transform.GetChild (26).gameObject.SetActive (false);//LEFT BOXES
			transform.GetChild (27).gameObject.SetActive (false);//End Game
			transform.GetChild (28).gameObject.SetActive (false);//Continue
			transform.GetChild (29).gameObject.SetActive (false);//Watch video and
			transform.GetChild (30).gameObject.SetActive (false);//Earn credits
			transform.GetChild (31).gameObject.SetActive (false);//total distance
			transform.GetChild (32).gameObject.SetActive (false);//one more time
			transform.GetChild (33).gameObject.SetActive (false);//progress bar
			transform.GetChild (34).gameObject.SetActive (false);//level display
			transform.GetChild (35).gameObject.SetActive (false);//level up pop up
			transform.GetChild (36).gameObject.SetActive (false);//Button ok
			transform.GetChild (37).gameObject.SetActive (false);//Price image
			transform.GetChild (38).gameObject.SetActive (false);//Reset Game
			transform.GetChild (40).gameObject.SetActive (false);//stage meter bar
			transform.GetChild (41).gameObject.SetActive (false);//Reset Game
			transform.GetChild (42).gameObject.SetActive (false);//Remaining Meters
			transform.GetChild (43).gameObject.SetActive (false);//medal to get 
			transform.GetChild (44).gameObject.SetActive (false);//how to play 
			transform.GetChild (45).gameObject.SetActive (false);//tutorial
			transform.GetChild (46).gameObject.SetActive (true);// GAMEPAD MODE
			transform.GetChild (47).gameObject.SetActive (false);// BUTTON PUSH 

			switch (globales.controlsMode) {
			case 0:
				transform.GetChild (46).gameObject.transform.GetChild (0).gameObject.SetActive (true);
				transform.GetChild (46).gameObject.transform.GetChild (1).gameObject.SetActive (false);
				transform.GetChild (46).gameObject.transform.GetChild (2).gameObject.SetActive (false);
				
				break;
			case 1:
				transform.GetChild (46).gameObject.transform.GetChild (0).gameObject.SetActive (false);
				transform.GetChild (46).gameObject.transform.GetChild (1).gameObject.SetActive (true);
				transform.GetChild (46).gameObject.transform.GetChild (2).gameObject.SetActive (false);
				
				break;
			case 2:
				transform.GetChild (46).gameObject.transform.GetChild (0).gameObject.SetActive (false);
				transform.GetChild (46).gameObject.transform.GetChild (1).gameObject.SetActive (false);
				transform.GetChild (46).gameObject.transform.GetChild (2).gameObject.SetActive (true);
				
				break;
			}
			transform.GetChild (48).gameObject.SetActive (false);// sine animation
			transform.GetChild (49).gameObject.SetActive (false);// sine animation
			transform.GetChild (50).gameObject.SetActive (false);// sine animation
			transform.GetChild (51).gameObject.SetActive (false);// sine animation
			transform.GetChild (52).gameObject.SetActive (false);// No, Go Back
			transform.GetChild (53).gameObject.SetActive (true);// Faces HUD

		}

		if (gameControl.currentState == gameControl.State.SETTINGS) {
			transform.GetChild (0).gameObject.SetActive (false); //Game Over
			transform.GetChild (1).gameObject.SetActive (false);//distance
			transform.GetChild (2).gameObject.SetActive (false);//jetski splash
			transform.GetChild (3).gameObject.SetActive (false);//play button
			transform.GetChild (4).gameObject.SetActive (false);//hud
			transform.GetChild (5).gameObject.SetActive (false);//world button
			transform.GetChild (6).gameObject.SetActive (true);//back button
			transform.GetChild (7).gameObject.SetActive (false);//winners button
			transform.GetChild (8).gameObject.SetActive (false);//select button
			transform.GetChild (9).gameObject.SetActive (false);//resume button
			transform.GetChild (10).gameObject.SetActive (false);//settings button

			//AUDIO
			transform.GetChild (11).gameObject.SetActive (true);//music button
			if (globales.sfxSwitch) {
				transform.GetChild (11).gameObject.transform.GetChild (1).gameObject.SetActive (true);
			} else {
				transform.GetChild (11).gameObject.transform.GetChild (1).gameObject.SetActive (false);

			}

			transform.GetChild (12).gameObject.SetActive (true);//sound button
			if (globales.musicSwitch) {
				transform.GetChild (12).gameObject.transform.GetChild (1).gameObject.SetActive (true);
			} else {
				transform.GetChild (12).gameObject.transform.GetChild (1).gameObject.SetActive (false);

			}

			//ADS
			if (!globales.goldenVersion) {
				transform.GetChild (13).gameObject.SetActive (false);//ads button
			} else {
				transform.GetChild (13).gameObject.SetActive (false);//ads button
				
			}

			transform.GetChild (14).gameObject.SetActive (false);//BOX 1
			transform.GetChild (15).gameObject.SetActive (false);//best run button
			transform.GetChild (16).gameObject.SetActive (false);//ads button
			transform.GetChild (17).gameObject.SetActive (false);//last record so far run button
			if (globales.controlsMode == 1) {
				transform.GetChild (18).gameObject.SetActive (true);//tooltip R button button
			} else {
				transform.GetChild (18).gameObject.SetActive (false);//tooltip R button button

			}
			
			if (globales.goldenVersion) {
				transform.GetChild (19).gameObject.SetActive (false);//golden version button
			} else {
				transform.GetChild (19).gameObject.SetActive (false);//golden version button

			}
			//RESTORE
			#if UNITY_IOS
			if (!globales.goldenVersion) {
				transform.GetChild (20).gameObject.SetActive (false);//restore button button ios
			} else {
				transform.GetChild (20).gameObject.SetActive (false);//restore button button ios
			}
			#endif
			
			#if UNITY_ANDROID
			transform.GetChild (20).gameObject.SetActive (false);//restore button button android
			#endif

			transform.GetChild (21).gameObject.SetActive (false);// OK! tutorial button
			transform.GetChild (22).gameObject.SetActive (false);// BOX 2
			transform.GetChild (23).gameObject.SetActive (false);//BOX
			transform.GetChild (24).gameObject.SetActive (false);//night night* text
			transform.GetChild (25).gameObject.SetActive (false);//FREE PLAY
			transform.GetChild (26).gameObject.SetActive (false);//LEFT BOXES
			transform.GetChild (27).gameObject.SetActive (false);//End Game
			transform.GetChild (28).gameObject.SetActive (false);//Continue
			transform.GetChild (29).gameObject.SetActive (false);//Watch video and
			transform.GetChild (30).gameObject.SetActive (false);//Earn credits
			transform.GetChild (31).gameObject.SetActive (false);//total distance
			transform.GetChild (32).gameObject.SetActive (false);//one more time
			transform.GetChild (33).gameObject.SetActive (false);//progress bar
			transform.GetChild (34).gameObject.SetActive (false);//level display
			transform.GetChild (35).gameObject.SetActive (false);//level up pop up
			transform.GetChild (36).gameObject.SetActive (false);//Button ok
			transform.GetChild (37).gameObject.SetActive (false);//Price image
			if (globales.isReseted) {
				transform.GetChild (38).gameObject.SetActive (false);//Reset Game
			} else {
				transform.GetChild (38).gameObject.SetActive (true);//Reset Game
			}
			transform.GetChild (40).gameObject.SetActive (false);//stage meter bar
			transform.GetChild (41).gameObject.SetActive (false);//LEVEL no se que vaciar
			transform.GetChild (42).gameObject.SetActive (false);//Remaining Meters
			transform.GetChild (43).gameObject.SetActive (false);//medal to get 
			transform.GetChild (44).gameObject.SetActive (false);//how to play 
			transform.GetChild (45).gameObject.SetActive (false);//tutorial
			// CONTROLES MODE
			transform.GetChild (46).gameObject.SetActive (true);// GAMEPAD MODE
			if (globales.controlsMode == 1) {

				transform.GetChild (47).gameObject.SetActive (false);// BUTTON PUSH 
			} else {
				transform.GetChild (47).gameObject.SetActive (false);// BUTTON PUSH 

			}

			switch (globales.controlsMode) {
			case 0:
				transform.GetChild (46).gameObject.transform.GetChild (0).gameObject.SetActive (true);
				transform.GetChild (46).gameObject.transform.GetChild (1).gameObject.SetActive (false);
				transform.GetChild (46).gameObject.transform.GetChild (2).gameObject.SetActive (false);
				transform.GetChild (46).gameObject.transform.GetChild (3).gameObject.SetActive (true);

				break;
			case 1:
				transform.GetChild (46).gameObject.transform.GetChild (0).gameObject.SetActive (false);
				transform.GetChild (46).gameObject.transform.GetChild (1).gameObject.SetActive (true);
				transform.GetChild (46).gameObject.transform.GetChild (2).gameObject.SetActive (false);
				transform.GetChild (46).gameObject.transform.GetChild (3).gameObject.SetActive (true);

				break;
			case 2:
				transform.GetChild (46).gameObject.transform.GetChild (0).gameObject.SetActive (false);
				transform.GetChild (46).gameObject.transform.GetChild (1).gameObject.SetActive (false);
				transform.GetChild (46).gameObject.transform.GetChild (2).gameObject.SetActive (true);
				transform.GetChild (46).gameObject.transform.GetChild (3).gameObject.SetActive (true);

				break;
			}
			transform.GetChild (48).gameObject.SetActive (false);// sine animation
			transform.GetChild (49).gameObject.SetActive (false);// sine animation
			transform.GetChild (50).gameObject.SetActive (false);// sine animation
			transform.GetChild (51).gameObject.SetActive (false);// sine animation
			transform.GetChild (52).gameObject.SetActive (false);// sine animation
			transform.GetChild (52).gameObject.SetActive (false);// No, Go Back
			transform.GetChild (53).gameObject.SetActive (false);// Faces HUD


		}

		if (gameControl.currentState == gameControl.State.RESET) {
			transform.GetChild (0).gameObject.SetActive (false); //Game Over
			transform.GetChild (1).gameObject.SetActive (false);//distance
			transform.GetChild (2).gameObject.SetActive (false);//jetski splash
			transform.GetChild (3).gameObject.SetActive (false);//play button
			transform.GetChild (4).gameObject.SetActive (false);//hud
			transform.GetChild (5).gameObject.SetActive (false);//world button
			transform.GetChild (6).gameObject.SetActive (false);//back button
			transform.GetChild (7).gameObject.SetActive (false);//winners button
			transform.GetChild (8).gameObject.SetActive (false);//select button
			transform.GetChild (9).gameObject.SetActive (false);//resume button
			transform.GetChild (10).gameObject.SetActive (false);//settings button
			transform.GetChild (11).gameObject.SetActive (false);//music button
			transform.GetChild (12).gameObject.SetActive (false);//sound button
			transform.GetChild (13).gameObject.SetActive (false);//ads button
			transform.GetChild (14).gameObject.SetActive (false);//BOX 1
			transform.GetChild (15).gameObject.SetActive (false);//best run button
			transform.GetChild (16).gameObject.SetActive (false);//ads button
			transform.GetChild (17).gameObject.SetActive (false);//last record so far run button
			transform.GetChild (18).gameObject.SetActive (false);//tooltip R button button
			transform.GetChild (19).gameObject.SetActive (false);//golden version button

			#if UNITY_ANDROID
			transform.GetChild (20).gameObject.SetActive (false);//restore button button android
			#endif

			transform.GetChild (21).gameObject.SetActive (false);// OK! tutorial button
			transform.GetChild (22).gameObject.SetActive (false);// BOX 2
			transform.GetChild (23).gameObject.SetActive (false);//BOX
			transform.GetChild (24).gameObject.SetActive (false);//night night* text
			transform.GetChild (25).gameObject.SetActive (false);//FREE PLAY
			transform.GetChild (26).gameObject.SetActive (false);//LEFT BOXES
			transform.GetChild (27).gameObject.SetActive (false);//End Game
			transform.GetChild (28).gameObject.SetActive (false);//Continue
			transform.GetChild (29).gameObject.SetActive (false);//Watch video and
			transform.GetChild (30).gameObject.SetActive (false);//Earn credits
			transform.GetChild (31).gameObject.SetActive (false);//total distance
			transform.GetChild (32).gameObject.SetActive (false);//one more time
			transform.GetChild (33).gameObject.SetActive (false);//progress bar
			transform.GetChild (34).gameObject.SetActive (false);//level display
			transform.GetChild (35).gameObject.SetActive (false);//level up pop up
			transform.GetChild (36).gameObject.SetActive (false);//Button ok
			transform.GetChild (37).gameObject.SetActive (false);//Price image
			transform.GetChild (38).gameObject.SetActive (false);//Reset Game

			transform.GetChild (40).gameObject.SetActive (false);//stage meter bar
			transform.GetChild (41).gameObject.SetActive (false);//LEVEL no se que vaciar
			transform.GetChild (42).gameObject.SetActive (false);//Remaining Meters
			transform.GetChild (43).gameObject.SetActive (false);//medal to get 
			transform.GetChild (44).gameObject.SetActive (false);//how to play 
			transform.GetChild (45).gameObject.SetActive (false);//tutorial
			transform.GetChild (46).gameObject.SetActive (false);// GAMEPAD MODE
			transform.GetChild (46).gameObject.transform.GetChild (1).gameObject.SetActive (false);
			transform.GetChild (47).gameObject.SetActive (false);// BUTTON PUSH 
			transform.GetChild (48).gameObject.SetActive (false);// sine animation
			transform.GetChild (49).gameObject.SetActive (true);// sine animation
			transform.GetChild (50).gameObject.SetActive (true);// sine animation
			transform.GetChild (51).gameObject.SetActive (true);// sine animation
			transform.GetChild (52).gameObject.SetActive (true);// sine animation
			transform.GetChild (52).gameObject.SetActive (false);// No, Go Back
			transform.GetChild (53).gameObject.SetActive (false);// Faces HUD



		}

		if (gameControl.currentState == gameControl.State.LEVELUP) {
			transform.GetChild (0).gameObject.SetActive (false);
			transform.GetChild (1).gameObject.SetActive (false);
			transform.GetChild (2).gameObject.SetActive (false);
			transform.GetChild (3).gameObject.SetActive (false);
			transform.GetChild (4).gameObject.SetActive (false);
			transform.GetChild (5).gameObject.SetActive (false);
			transform.GetChild (6).gameObject.SetActive (false);//back button
			transform.GetChild (7).gameObject.SetActive (false);//winners button
			transform.GetChild (8).gameObject.SetActive (false);//pause button
			transform.GetChild (9).gameObject.SetActive (false);//resume button
			transform.GetChild (10).gameObject.SetActive (false);//settings button
			transform.GetChild (11).gameObject.SetActive (false);//music button
			transform.GetChild (12).gameObject.SetActive (false);//sound button
			transform.GetChild (13).gameObject.SetActive (false);//ads button
			transform.GetChild (14).gameObject.SetActive (true);//LevelUP_next stage button
			transform.GetChild (15).gameObject.SetActive (false);//best run button
			transform.GetChild (16).gameObject.SetActive (false);//ads button
			transform.GetChild (17).gameObject.SetActive (false);//last record so far run button
			transform.GetChild (18).gameObject.SetActive (false);//tooltip R button button
			transform.GetChild (19).gameObject.SetActive (false);//Falda
			transform.GetChild (20).gameObject.SetActive (false);//restore button button
			transform.GetChild (21).gameObject.SetActive (false);// OK! tutorial button
			transform.GetChild (22).gameObject.SetActive (false);// BOX 2
			transform.GetChild (23).gameObject.SetActive (false);//BOX
			transform.GetChild (24).gameObject.SetActive (false);//night night* text
			transform.GetChild (25).gameObject.SetActive (false);//FREE PLAY
			transform.GetChild (26).gameObject.SetActive (false);//LEFT BOXES
			transform.GetChild (27).gameObject.SetActive (false);//End Game
			transform.GetChild (28).gameObject.SetActive (false);//Continue
			transform.GetChild (29).gameObject.SetActive (false);//Watch video and
			transform.GetChild (30).gameObject.SetActive (false);//Earn credits
			transform.GetChild (31).gameObject.SetActive (false);//total distance
			transform.GetChild (32).gameObject.SetActive (false);//one more time
			transform.GetChild (33).gameObject.SetActive (false);//progress bar
			transform.GetChild (34).gameObject.SetActive (false);//level display
			transform.GetChild (35).gameObject.SetActive (false);//congrats
			transform.GetChild (36).gameObject.SetActive (false);//Button ok
			transform.GetChild (37).gameObject.SetActive (false);//Price image
			transform.GetChild (38).gameObject.SetActive (false);//Reset Game
			transform.GetChild (40).gameObject.SetActive (false);//stage meter bar
			transform.GetChild (41).gameObject.SetActive (false);//Reset Game
			transform.GetChild (42).gameObject.SetActive (false);//Remaining Meters
			transform.GetChild (43).gameObject.SetActive (false);//medal to get 
			transform.GetChild (44).gameObject.SetActive (false);//how to play 
			transform.GetChild (45).gameObject.SetActive (false);//tutorial
			transform.GetChild (46).gameObject.SetActive (false);// GAMEPAD MODE
			transform.GetChild (48).gameObject.SetActive (false);// sine animation
			transform.GetChild (49).gameObject.SetActive (false);// sine animation
			transform.GetChild (50).gameObject.SetActive (false);// sine animation
			transform.GetChild (51).gameObject.SetActive (false);// sine animation
			transform.GetChild (52).gameObject.SetActive (false);// No, Go Back
			transform.GetChild (53).gameObject.SetActive (false);// Faces HUD

			
		}

		if (gameControl.currentState == gameControl.State.TUTORIAL) {
			transform.GetChild (0).gameObject.SetActive (false);
			transform.GetChild (1).gameObject.SetActive (false);
			transform.GetChild (2).gameObject.SetActive (false);
			transform.GetChild (3).gameObject.SetActive (false);
			transform.GetChild (4).gameObject.SetActive (false);
			transform.GetChild (5).gameObject.SetActive (false);
			transform.GetChild (6).gameObject.SetActive (false);//back button
			transform.GetChild (7).gameObject.SetActive (false);//winners button
			transform.GetChild (8).gameObject.SetActive (false);//pause button
			transform.GetChild (9).gameObject.SetActive (false);//resume button
			transform.GetChild (10).gameObject.SetActive (false);//settings button
			transform.GetChild (11).gameObject.SetActive (false);//music button
			transform.GetChild (12).gameObject.SetActive (false);//sound button
			transform.GetChild (13).gameObject.SetActive (false);//ads button
			transform.GetChild (14).gameObject.SetActive (false);//ads button
			transform.GetChild (15).gameObject.SetActive (false);//best run button
			transform.GetChild (16).gameObject.SetActive (false);//ads button
			transform.GetChild (17).gameObject.SetActive (false);//last record so far run button
			transform.GetChild (18).gameObject.SetActive (false);//tooltip R button button
			transform.GetChild (19).gameObject.SetActive (false);//golden version button
			transform.GetChild (20).gameObject.SetActive (false);//restore button button
			transform.GetChild (21).gameObject.SetActive (false);// OK! tutorial button
			transform.GetChild (22).gameObject.SetActive (false);// BOX 2
			transform.GetChild (23).gameObject.SetActive (false);//BOX
			transform.GetChild (24).gameObject.SetActive (false);//night night* text
			transform.GetChild (25).gameObject.SetActive (false);//FREE PLAY
			transform.GetChild (26).gameObject.SetActive (false);//LEFT BOXES
			transform.GetChild (27).gameObject.SetActive (false);//End Game
			transform.GetChild (28).gameObject.SetActive (false);//Continue
			transform.GetChild (29).gameObject.SetActive (false);//Watch video and
			transform.GetChild (30).gameObject.SetActive (false);//Earn credits
			transform.GetChild (31).gameObject.SetActive (false);//total distance
			transform.GetChild (32).gameObject.SetActive (false);//one more time
			transform.GetChild (33).gameObject.SetActive (false);//progress bar
			transform.GetChild (34).gameObject.SetActive (false);//level display
			transform.GetChild (35).gameObject.SetActive (false);//level up pop up
			transform.GetChild (36).gameObject.SetActive (true);//Button ok
			transform.GetChild (37).gameObject.SetActive (false);//Price image
			transform.GetChild (38).gameObject.SetActive (false);//Reset Game
			transform.GetChild (40).gameObject.SetActive (false);//stage meter bar
			transform.GetChild (41).gameObject.SetActive (false);//Reset Game
			transform.GetChild (42).gameObject.SetActive (false);//Remaining Meters
			transform.GetChild (43).gameObject.SetActive (false);//medal to get 
			transform.GetChild (44).gameObject.SetActive (false);//how to play
			transform.GetChild (45).gameObject.SetActive (true);//tutorial
			transform.GetChild (46).gameObject.SetActive (false);// GAMEPAD MODE
			transform.GetChild (48).gameObject.SetActive (false);// sine animation
			transform.GetChild (49).gameObject.SetActive (false);// sine animation
			transform.GetChild (50).gameObject.SetActive (false);// sine animation
			transform.GetChild (51).gameObject.SetActive (false);// sine animation
			transform.GetChild (52).gameObject.SetActive (false);// No, Go Back
			transform.GetChild (53).gameObject.SetActive (false);// Faces HUD

			
		}

	}
		
		
	//		public void isLeftTutorialPressed ()
	//		{
	//				if (isLeftPressedsingle) {
	//						isLeftPressed = true;
	//				} else {
	//						isLeftPressedsingle = true;
	//				}
	//		}
	//
	//		public void isRightTutorialPressed ()
	//		{
	//				if (isRightPressedsingle) {
	//						isRightPressed = true;
	//				} else {
	//						isRightPressedsingle = true;
	//				}
	//		}
	public void buyNoAds ()
	{
//				Soomla.Store.StoreInventory.BuyItem (Soomla.Store.GameAssets.REMOVE_ADS_ITEM_ID as string);
//				foreach (Soomla.Store.VirtualGood vg in Soomla.Store.StoreInfo.Goods) {
//			
//						Debug.Log ("SOOMLA/UNITY wants to buy: " + vg.Name);
//						try {
//								Soomla.Store.StoreInventory.BuyItem (vg.ItemId);
//
//						} catch (Exception e) {
//								Debug.LogError ("SOOMLA/UNITY " + e.Message);
//						}
//				}




	}


	public void buttonsnd ()
	{
		SoundManager.playButtontClip ();

//				print ("BUTTON PRESSED");
	}




	public void restoreTransactions ()
	{
//				Soomla.Store.SosomlaStore.RestoreTransactions ();
//				Soomla.Store.SoomlaStore.RefreshInventory ();
	}
	
}
		

