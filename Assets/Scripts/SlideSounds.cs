﻿using UnityEngine;
using System.Collections;

public class SlideSounds : MonoBehaviour
{

		AudioSource soundPlayer;
		private bool played;

		public  AudioClip myClip;
		public  AudioClip pog;


		public bool Played {
				get { return played; }
				set {
						played = value;
				}
		}

		// Use this for initialization
		void Start ()
		{
				soundPlayer = GetComponent<AudioSource> ();
				played = false;
//				samplerate = RandomNumber.Next (440, 48000);
//				myClip = AudioClip.Create ("MySinusoid", samplerate, 1, samplerate, true, OnAudioRead, OnAudioSetPosition);
//				pog = AudioClip.Create ("pog", samplerate * 2, 1, samplerate / 2, true, OnAudioRead, OnAudioSetPosition);
		}
	
	
//		private int getSamplerate ()
//		{
//				samplerate = RandomNumber.Next (440, 48000);
//				return samplerate;
//		}
		public  void PlaySlideSound ()
		{
				soundPlayer.clip = myClip;
				if (!soundPlayer.isPlaying && !played) {
						soundPlayer.pitch = Random.Range (0.1f, 1);
//						soundPlayer.volume = 0.04f; 
						soundPlayer.Play ();
						played = true;
				}

		}
	
		public  void PlayPogSound ()
		{
				soundPlayer.clip = pog;
				if (!soundPlayer.isPlaying && !played) {
						soundPlayer.pitch = Random.Range (0.1f, 1);
//						soundPlayer.volume = 0.04f; 
						soundPlayer.Play ();
						played = true;

				}
		}
	
//		void OnAudioRead (float[] data)
//		{
//				int count = 0;
//				samplerate = getSamplerate ();
//				while (count < data.Length) {
//						data [count] = Mathf.PerlinNoise (frequency * position / samplerate, 0);
//						samplerate++;
//			
//						position = position + 1;
//						count = count + 1;
//				}
//		
//		}
//	
//		void OnAudioSetPosition (int newPosition)
//		{
//				position = newPosition;
//		}
}
