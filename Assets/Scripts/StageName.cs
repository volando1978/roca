﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using SmartLocalization;


public class StageName : MonoBehaviour
{
	
	public float letterPause = 1f;
	
	public List<string> stageNames = new List<string> ();

	LanguageManager localizator;
	string message;
	gameControl controller;

	bool running = false;


	public void Awake ()
	{
				
		LanguageManager localizator = LanguageManager.Instance;
		message = localizator.GetTextValue ("entering").ToString ();
		controller = GameObject.FindGameObjectWithTag ("GameController").GetComponent<gameControl> ();

	}

	public void Start ()
	{
		
	}


	// Update is called once per frame
	public  void setStageName ()
	{
		if (!running) {
			StartCoroutine (TypeText ());
		}
	}

	
	IEnumerator TypeText ()
	{
		running = true;
		GetComponent<Text> ().text = "";
//		print (" MESS " + message);
		if (message == null) {
			message = localizator.GetTextValue ("entering");
		}

		foreach (char letter in message.ToCharArray()) {
			GetComponent<Text> ().text += letter;
			SoundManager.playTypeWriter ();
				
			yield return new WaitForSeconds (0);
		}
		yield return new WaitForSeconds (1);
			
		message = stageNames [globales.currentChallengeIndex] + " " + controller.level;
		GetComponent<Text> ().text = "";
			
		foreach (char letter in message.ToCharArray()) {
			GetComponent<Text> ().text += letter;
			SoundManager.playTypeWriter ();

			yield return new WaitForSeconds (0);
				
		}
		running = false;
	}
	
	
}

