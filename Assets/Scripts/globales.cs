using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;

//using Soomla;

public class globales : MonoBehaviour
{
	
	[SerializeField]
	int
		frameRate = 60;

	public static int hview;
	public static int wview;

	// GAME POINTS
	public static int numberOfGames;
	public static int currentChallengeIndex;
	public static int scorePoints;
	public static int contadorChallenge;
	public static int contadorChallengeAkku;
	public static int maxDistance;
	public static int maxDistanceNight;
	public static int totalLifeMeters;

	//PROGRESSION
	public static int nMinions;
	public static int lastTotalLifeMeters;
	public static int numberOfGameLevels;



	public static int lastGamePoints;
	public static float speedHUD;

	public static float camWidth;
	public static float camHeight;

	public static float WIDTH;
	public static float HEIGHT;
	public static float SCREENH;
	public static float SCREENW;

	[Range (0, 4)]
	public float
		changeColorTime;
	
	public static bool isLandscape;
	public static bool ISWIDE;

	public static bool showAdApple;
	public static bool showUnity;

	public static bool shaking = false;
	public static bool showNewRecord = false;
	public static bool showNewRecordNight = false;
	public static bool gameNight = false;
	public static bool passStage = false;
	//		public Texture2D imageToShare;
		
	public static int controlsMode = 1;
	public static bool isReseted = false;


	//AUDIO SWITCHERS
	public static bool sfxSwitch;
	public static bool musicSwitch;
	public static int meterScore = 0;


	[SerializeField]
	public static bool
		collisionsOn = true;

	[SerializeField]
	public static bool
		dead;
	public bool deadTally;

	//		private static Soomla.Store.EventHandler handler;


	//SCREEN
	public static Vector2 SCREENVECTOR;
	public static Vector2 SCREENSCALE;

	static List<Matrix4x4> stack;
		
	//ADVERTISING
	public static bool goldenVersion;

	//SOCIAL
	//		public static Reward exampleReward = new BadgeReward ("example_reward", "Example Social Reward");


	//CONTINUES
	public static Vector3 coordContinue;
	public static bool playerFelt = false;
	public static bool isContinue = true;

	[SerializeField]
	public static bool[]
		challengeIndex = new bool[40];

	public List<Color32> colors = new List<Color32> ();
	public static Color32 currentColor;

	public void Awake ()
	{
				
		Application.targetFrameRate = frameRate;// -1;
		getData ();
		setCamera ();
		stack = new List<Matrix4x4> ();
		goldenVersion = false;
//				print ("SFX " + sfxSwitch + " MUSIC " + musicSwitch);
		soundCheck ();
//				print ("cual es " + currentChallengeIndex);
		checkChallengeIndex ();
		numberOfGameLevels = 40;
	}

	//SHOW FPS
	//		void OnGUI ()
	//		{
	//				GUI.Label (new Rect (0, 0, 100, 100), ((int)(1.0f / Time.smoothDeltaTime)).ToString ());
	//		}

	
	void checkChallengeIndex ()
	{
		for (int i = 0; i < challengeIndex.Length; i++) {
			if (i <= currentChallengeIndex) {
				challengeIndex [i] = true;
			} else {
				challengeIndex [i] = false;
			}
		}

	}

	// Use this for initialization
	void Start ()
	{
//		ProgController.CheckProgresionAndLevel ();

		setCamera ();		
		showAdApple = false;
//				handler = new Soomla.Store.EventHandler ();//for android check!
//				Soomla.Store.StoreEvents.OnSoomlaStoreInitialized += onSoomlaStoreInitialized;
//				Soomla.Store.SoomlaStore.Initialize (new Soomla.Store.GameAssets ());
//				SPTwitter.instance.Init ();

		
	}

	IEnumerator morphCam ()
	{

		GameObject surface = GameObject.FindGameObjectWithTag ("Surface");
		
		currentColor = colors [currentChallengeIndex];


		while (surface.GetComponent<Renderer> ().material.color != currentColor) {
			surface.GetComponent<Renderer> ().material.color = Color.Lerp (surface.GetComponent<Renderer> ().material.color, currentColor, Time.deltaTime * changeColorTime);
			Camera.main.backgroundColor = Color.Lerp (Camera.main.backgroundColor, currentColor, Time.deltaTime * changeColorTime);


			//FOG
//						RenderSettings.fogDensity = Mathf.Lerp (bottomDensity, topDensity, (transform.position.y - bottomHeight) / heightDif);
//						RenderSettings.fogColor = Color.Lerp (bottomColor, topColor, (transform.position.y - bottomHeight) / heightDif);

//						RenderSettings.fogDensity = Mathf.Lerp (bottomDensity, topDensity, (transform.position.y - bottomHeight) / heightDif);
			RenderSettings.fogColor = Color.Lerp (Camera.main.backgroundColor, currentColor, Time.deltaTime * changeColorTime);

//						print ("coroutine colorreeee " + Camera.main.backgroundColor + " currentColor " + currentColor + " fogColor " + RenderSettings.fogColor);
			yield return null;
		}

	}

	public void setColor ()
	{
		StartCoroutine ("morphCam");
	}





	
	static public void BeginGUI ()
	{
		stack.Add (GUI.matrix);
		Matrix4x4 m = new Matrix4x4 ();
		var w = (float)SCREENW;
		var h = (float)SCREENH;
		var aspect = w / h;
		var scale = 1f;
		var offset = Vector3.zero;
		if (aspect < (WIDTH / HEIGHT)) { //screen is taller
			scale = (SCREENW / WIDTH);
			offset.y += (SCREENH - (HEIGHT * scale)) * 0.5f;
		} else { // screen is wider
			scale = (SCREENH / HEIGHT);
			offset.x += (SCREENW - (WIDTH * scale)) * 0.5f;
		}
		m.SetTRS (offset, Quaternion.identity, Vector3.one * scale);
		GUI.matrix *= m;
	}

	static public void EndGUI ()
	{
		GUI.matrix = stack [stack.Count - 1];
		stack.RemoveAt (stack.Count - 1);
	}

	public static bool checkPlayersMissing ()
	{
		bool m = true;
		GameObject[] players = GameObject.FindGameObjectsWithTag ("Player");
		if (players.Length < 2) {
			m = false;
		}
		return m;
	}

	public static void setCamera ()
	{

		globales.camHeight = Camera.main.orthographicSize * 2f;   
		globales.camWidth = camHeight * Camera.main.aspect;
		
		
		// Calculations assume map is position at the origin
		SCREENW = Screen.width;
		SCREENH = Screen.height;
		WIDTH = SCREENW;
		HEIGHT = SCREENH;
		SCREENVECTOR = new Vector2 (SCREENW, SCREENH);
		SCREENSCALE = new Vector2 (SCREENW / WIDTH, SCREENH / HEIGHT);
	}

	//		public void onSoomlaStoreInitialized ()
	//		{
	//
	//				print ("Soomla inicializada");
	////				print ("GET VALUE STORE " + Soomla.Store.StoreInventory.GetItemBalance (Soomla.Store.GameAssets.REMOVE_ADS_GOOD.ItemId));
	//		
	//				int val = Soomla.Store.StoreInventory.GetItemBalance (Soomla.Store.GameAssets.REMOVE_ADS_GOOD.ItemId);
	//		
	//				if (val < 1) {
	//						globales.goldenVersion = false;
	////						print ("IS PLATA");
	//				} else {
	//						globales.goldenVersion = true;
	////						print ("IS GOLDEN");
	//			
	//				}
	//		
	//		}



	
	// Update is called once per frame
	void Update ()
	{

		deadTally = dead;
		#if UNITY_EDITOR 
		
		if (Input.GetKeyDown (KeyCode.P)) {
//						deleteData ();
			SoundManager.playTypeWriter ();

		}

		if (Input.GetKeyDown (KeyCode.O)) {
			//						deleteData ();
			SoundManager.playCoini ();
			
		}
		
		#endif
	}

	

	
	
	//--PLAYERPREFS
	
	public static void setData ()
	{

		PlayerPrefs.SetInt ("numberOfGames", globales.numberOfGames);
		PlayerPrefs.SetInt ("maxDistance", globales.maxDistance);
		PlayerPrefs.SetInt ("maxDistanceNight", globales.maxDistanceNight);
		PlayerPrefs.SetInt ("totalLifeMeters", globales.totalLifeMeters);
		PlayerPrefs.SetInt ("lastTotalLifeMeters", globales.lastTotalLifeMeters);
		PlayerPrefs.SetInt ("totalLifeMeters", globales.totalLifeMeters);
		PlayerPrefs.SetInt ("currentChallengeIndex", globales.currentChallengeIndex);
		PlayerPrefs.SetInt ("contadorChallengeAkku", globales.contadorChallengeAkku);
		PlayerPrefs.SetInt ("controlsMode", globales.controlsMode);

		setSoundData ();

		print ("SAVE");

	}

	public static void setSoundData ()
	{


		SetBool ("sfxSwitch", sfxSwitch);
		SetBool ("musicSwitch", musicSwitch);
//				print ("SAVE SOUND " + sfxSwitch + " SAVE MUSIC " + musicSwitch);

		
	}

	void getData ()
	{

		print ("GETTING DATA");
		if (PlayerPrefs.HasKey ("numberOfGames")) {
			numberOfGames = PlayerPrefs.GetInt ("numberOfGames");
			Debug.Log ("loading... numberOfGames: " + numberOfGames);
		}

		if (PlayerPrefs.HasKey ("maxDistance")) {
			globales.maxDistance = PlayerPrefs.GetInt ("maxDistance");
			Debug.Log ("loading....maxDistance: " + globales.maxDistance);
		}

		if (PlayerPrefs.HasKey ("maxDistanceNight")) {
			globales.maxDistanceNight = PlayerPrefs.GetInt ("maxDistanceNight");
			Debug.Log ("loading....maxDistanceNight: " + globales.maxDistanceNight);
		}
		if (PlayerPrefs.HasKey ("totalLifeMeters")) {
			globales.totalLifeMeters = PlayerPrefs.GetInt ("totalLifeMeters");
			Debug.Log ("loading....totalLifeMeters: " + globales.totalLifeMeters);
		}
		if (PlayerPrefs.HasKey ("lastTotalLifeMeters")) {
			globales.lastTotalLifeMeters = PlayerPrefs.GetInt ("lastTotalLifeMeters");
			Debug.Log ("loading....lastTotalLifeMeters: " + globales.lastTotalLifeMeters);
		}

		if (PlayerPrefs.HasKey ("currentChallengeIndex")) {
			globales.currentChallengeIndex = PlayerPrefs.GetInt ("currentChallengeIndex");
			Debug.Log ("loading....currentChallengeIndex: " + globales.currentChallengeIndex);
		}

		if (PlayerPrefs.HasKey ("contadorChallengeAkku")) {
			globales.contadorChallengeAkku = PlayerPrefs.GetInt ("contadorChallengeAkku");
			Debug.Log ("loading....contadorChallengeAkku: " + globales.contadorChallengeAkku);
		}

		if (PlayerPrefs.HasKey ("controlsMode")) {
			globales.contadorChallengeAkku = PlayerPrefs.GetInt ("controlsMode");
			Debug.Log ("loading....controlsMode: " + globales.controlsMode);
		}

		if (PlayerPrefs.HasKey ("sfxSwitch")) {
			sfxSwitch = GetBool ("sfxSwitch");
			Debug.Log ("loading... sfxSwitch: " + sfxSwitch);
		}

		if (PlayerPrefs.HasKey ("musicSwitch")) {
			musicSwitch = GetBool ("musicSwitch");
			Debug.Log ("loading... musicSwitch: " + musicSwitch);
		}

	}

	
	public void deleteData ()
	{
		PlayerPrefs.DeleteAll ();

		numberOfGames = -2;
		globales.maxDistance = 0;
		globales.maxDistanceNight = 0;
		globales.totalLifeMeters = 0;
		globales.lastTotalLifeMeters = 0;
		globales.contadorChallenge = 0;
		globales.contadorChallengeAkku = 0;
				
		globales.currentChallengeIndex = 0;
		checkChallengeIndex ();
//		ProgController.CheckProgresionAndLevel ();

//		setColor ();
		PlayerPrefs.Save ();
//				controlsMode = 0;
		isReseted = true;

		gameObject.GetComponent<gameControl> ().toMenu ();

		print (" DELETING __ ******** LEVEL " + " contadorChallenge " + globales.contadorChallenge + " ChallengeLevel " + globales.currentChallengeIndex + " maxDistance " + globales.maxDistance + " meterScore " + globales.scorePoints + " passing " + globales.passStage);
	}

	
	public void OnApplicationQuit ()
	{
//				setData ();
		Destroy (gameObject);
	}
	
	
	//herramientas para grabar tipo bool
	static void  SetBool (string name, bool value)
	{
		
		PlayerPrefs.SetInt (name, value ? 1 : 0);
	}

	static  bool GetBool (string name)
	{
		return PlayerPrefs.GetInt (name) == 1 ? true : false;
	}

	static  bool GetBool (string name, bool defaultValue)
	{
		if (PlayerPrefs.HasKey (name)) {
			return GetBool (name);
		}
		return defaultValue;
	}

	
	public static IEnumerator sleep (float t)
	{
//				print ("ENTRA SLEEP");
		yield return new WaitForSeconds (t);
		yield return null;
	}

	
	public static void clearMenu ()
	{
		GameObject[] gos = GameObject.FindGameObjectsWithTag ("Menu");
		foreach (GameObject go in gos) {
			Destroy (go);
		}
	}

	public void changeControlMode ()
	{
		controlsMode += 1;
		if (controlsMode >= 3) {
			controlsMode = 0;
		}
		print ("controlmode is " + controlsMode);
	}

	
	#region HERRAMIENTAS

	public static Vector2 getRandomPos ()
	{
		
		float rx = Random.Range (0.1f, 0.9f);
		float ry = Random.Range (0.1f, 0.9f);
		
		Vector2 rPos = (Vector2)Camera.main.ViewportToWorldPoint (new Vector3 (rx, ry, 0));
		return rPos;
		
	}

	public static Vector3 getRandomRot ()
	{
		
		float rx = Random.Range (0.1f, 0.9f);
		float ry = Random.Range (0.1f, 0.9f);
		float rz = Random.Range (0f, 360f);
		
		Vector3 rRot = new Vector3 (rx, ry, rz);
		
		return rRot;
		
	}

	public static void setCameraLevelColor ()
	{
		Color32 r = Camera.main.backgroundColor;
		
		r.r = (byte)(r.r + Random.Range (-10, 10));
		r.g = (byte)(r.g + Random.Range (-10, 10));
		r.b = (byte)(r.b + Random.Range (-10, 10));
		r.a = (byte)(r.a + Random.Range (-10, 10));
		
		Camera.main.backgroundColor = r;
	}

	public static void setCameraRandomColor ()
	{
		Color32 r;
		
		r.r = (byte)Random.Range (10, 128);
		r.g = (byte)Random.Range (10, 128);
		r.b = (byte)Random.Range (10, 128);
		r.a = (byte)Random.Range (10, 128);
		
		Camera.main.backgroundColor = r;
	}


	
	public static Color getRandomColor ()
	{
		Color32 r;
		
		r.r = (byte)Random.Range (150, 255);
		r.g = (byte)Random.Range (150, 255);
		r.b = (byte)Random.Range (150, 255);
		r.a = 255;//(byte)Random.Range (128, 128);
		
		
		return (Color)r;
	}

	public static void setGrey ()
	{
		
		Color32 r;
		
		byte n = (byte)85;
		r.r = n;
		r.g = n;
		r.b = n;
		r.a = n;
		
		Camera.main.backgroundColor = r;
		
		
	}

	public static void lanzaEnDirecciones (GameObject go)
	{
		ArrayList abec = new ArrayList ();
		float len = 10f;
		
		if (go) {
			
			for (int i = 0; i <= 360; i += 120) {
				
				GameObject option = (GameObject)Instantiate (go);
				abec.Add (option);
				float xD = Mathf.Cos ((float)i * Mathf.Deg2Rad);
				float yD = Mathf.Sin ((float)i * Mathf.Deg2Rad);
				
				Vector3 pos = new Vector3 (option.transform.position.x + len * xD, option.transform.position.y + len * yD, 0);
				option.transform.position = Vector3.Lerp (option.transform.position, pos, Time.deltaTime * 100f);
				
			}
		}
		
		
	}

	#endregion

	//AUDIO TOGGLE!!
	public  void audioToggle ()
	{
		if (sfxSwitch) {
			sfxSwitch = false;
			SoundManager.playButtontClip ();// FEEDBACK SOUND
			soundCheck ();
		} else {
			sfxSwitch = true;
			soundCheck ();	
		}
	}

	public  void musicToggle ()
	{
		if (musicSwitch) {
			musicSwitch = false; //inverted
			soundCheck ();
		} else {
			musicSwitch = true;
			soundCheck ();	
		}
	}

	public  void soundCheck ()
	{
		GameObject sound = GameObject.FindGameObjectWithTag ("soundManager");
		GameObject music = GameObject.FindGameObjectWithTag ("music");
		
		if (sound) {
			sound.GetComponent<AudioSource> ().mute = sfxSwitch;
			print (sfxSwitch);
		}

		
		if (music) {
			music.GetComponent<AudioSource> ().mute = musicSwitch;
			
		}
		globales.setData ();
	}

	public static int RandomSign ()
	{
		return Random.value < .5 ? 1 : -1;
	}

	public void tweet ()
	{

		#region SOOMLA_TWEET

//				Soomla.Profile.SoomlaProfile.UpdateStory (
//			twitterProvider,
//			"HOHOHO",
//			"The Story of ",
//			"YO",
//			"DESCRIPT",
//			"http://www.screenimplosion.com",
//			"http://screenimplosion.com/wp-content/uploads/2015/05/Screen-Shot-2015-04-05-at-13.14.38-e1430474931854.png",
//			null,
//			exampleReward);
//
//				print (exampleReward + " " + twitterProvider + " tweet");
		#endregion

		#region HTTP_TWEET
//				const string Address = "http://twitter.com/intent/tweet";
//				string text = "This is my record at Downhill : " + globales.maxDistance + " METERS\n Can you beat me? #downhill";
//				string picture = "https://dl.dropboxusercontent.com/u/4351226/icon.png";
//				string related = "http://www.twitter.com/screenimplosion";
//				string url = " brought to you by: www.screenimplosion.com";
//				string lang = "en";
//				Application.OpenURL (Address +
//						"?text=" + WWW.EscapeURL (text) +
//						"&amp;url=" + WWW.EscapeURL (url) +
//						"&amp;related=" + WWW.EscapeURL (related) +
//						"&amp;picture=" + WWW.EscapeURL (picture) +
//						"&amp;lang=" + WWW.EscapeURL (lang));
		#endregion

		#region PLUGIN_HOMEMADE 
//				Tweet.TweetTapped ();
		#endregion

		#region PLUGIN_DE_PAGO
//				imageToShare = Application.CaptureScreenshot ("screen.png");
//				imageToShare = (Texture2d)imageToShare;
//				SPShareUtility.TwitterShare ("I did a #surreal #downhill of : " + globales.maxDistance + " meters" + "\nCan you beat it? It's #free goo.gl/1WVeyD");
//				SPShareUtility.TwitterShare ("This is my record today playing Downhill: ");
		#endregion

	
	}

}
