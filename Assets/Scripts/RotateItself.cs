﻿using UnityEngine;
using System.Collections;

public class RotateItself : MonoBehaviour
{
	public int rotationSpeed;

	
	// Update is called once per frame
	void Update ()
	{
	
		transform.Rotate (Vector3.up * rotationSpeed * Time.deltaTime);
	}
}
