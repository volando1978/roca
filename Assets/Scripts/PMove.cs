﻿using UnityEngine;
using System.Collections;

public class PMove : MonoBehaviour
{

	public GameObject boxes;
	float speed;
	Rigidbody rigidbody;
	bool collided = false;
	public int timerDeath;

	void Awake ()
	{
		rigidbody = GetComponent<Rigidbody> ();
	}

	void OnCollisionEnter (Collision c)
	{


		if (c.gameObject.tag == "DeathBox") {

			print ("whos colliding: " + GetComponent<Collider> ().gameObject.name);
			Destroy (gameObject);

		}

		if (c.gameObject.tag == "Piedra") {
			collided = true;
			StartCoroutine ("CheckDeath");

		}

	}



	IEnumerator CheckDeath ()
	{
		float t = 0;
//		print (gameObject + " " + t);

		while (t < timerDeath) {
			t += 1 * Time.deltaTime;
//			print (t);
			yield return new WaitForSeconds (0);


			if (t >= timerDeath) {
				StopCoroutine ("CheckDeath");
				Destroy (gameObject);
			}

		}
	}

	bool CheckMov ()
	{
		bool mov = true;
		speed = rigidbody.velocity.magnitude;
		if (speed < 0.5 && collided) {
			rigidbody.velocity = new Vector3 (0, 0, 0);
			gameObject.GetComponent<MeshRenderer> ().material.color = Color.red;

			mov = false;
		}

		return mov;
	}



	void Update ()
	{
		if (!CheckMov () && collided) {
//			print ("no checkmov");
			StartCoroutine ("CheckDeath");
		}
	}
}
