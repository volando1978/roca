﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class DropAnim : MonoBehaviour
{


	public float spd;

	gameControl controller;



	void Awake ()
	{
		controller = GameObject.FindGameObjectWithTag ("GameController").GetComponent<gameControl> ();

	}


	public void DropButton ()
	{
		StopCoroutine ("f");

		StartCoroutine ("d");
	}

	public void LiftButton ()
	{
		StopCoroutine ("d");

		StartCoroutine ("f");
	}


	IEnumerator d ()
	{
		float start = transform.localScale.y;
		float startTime = Time.time;
		float overTime = 0.5f;


		while (transform.localScale.y > 0) {
			float reduction = Mathf.Lerp (transform.localScale.y, 0, (Time.time - startTime) / overTime);

			Vector3 pos = new Vector3 (reduction, reduction, reduction);
			transform.localScale = pos;
//			print ("Coroutine d DropAnim");


			yield return null;
			if (transform.localScale.y <= 0) {
				StopCoroutine ("d");
			}
		}


		yield return null;
	}

	IEnumerator f ()
	{
		float start = transform.localScale.y;
		float startTime = Time.time;
		float overTime = 0.5f;


		while (transform.localScale.y < 1) {
			float enlarge = Mathf.Lerp (transform.localScale.y, 1, (Time.time - startTime) / overTime);
			Vector3 pos = new Vector3 (enlarge, enlarge, enlarge);
			transform.localScale = pos;
//			print ("Coroutine f DropAnim");


			yield return null;
			if (transform.localScale.y >= 1) {
				StopCoroutine ("f");
			}
		}


		yield return null;
	}
}
