﻿using UnityEngine;
using System.Collections;

public class GrowSphere : MonoBehaviour
{


		float grow;
		[Range(10,3800)]
		public float
				maxGrow;
		public float growRatio;
		bool shrinking;


		// Use this for initialization
		void Start ()
		{
				grow = 0.1f;
				GetComponent<Renderer> ().material.color = Color.black;
				shrinking = false;
		}
	
		// Update is called once per frame
		void Update ()
		{

				if (grow < maxGrow && ! shrinking) {
						grow += growRatio;
						transform.localScale = new Vector3 (grow, 10f, grow);
				}


				if (transform.localScale.x < 0) {
						Destroy (gameObject);
				}
		}

		public void goShrink ()
		{
				StartCoroutine (Shrink ());
		}

		public IEnumerator Shrink ()
		{
				shrinking = true;
				while (transform.localScale.x > 0) {
//						print ("rutina shirbg " + grow);
						grow -= growRatio * 6;
						transform.localScale = new Vector3 (grow, 0.1f, grow);
						yield return new WaitForSeconds (0);

				}
		
		

		}
}
