﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Colorinchos : MonoBehaviour
{

		Text myText;
		bool triggeredCoroutine;


		// Use this for initialization
		void Start ()
		{
				myText = GetComponent<Text> ();
				triggeredCoroutine = false;

		}

		private IEnumerator coloring ()
		{
//				print ("GOAAAAA " + gameObject.activeSelf);

				while (gameControl.currentState == gameControl.State.GAMEOVER) {
						yield return new WaitForSeconds (0.5f * Time.deltaTime);
//						print ("paining " + gameObject.activeSelf);
						myText.color = globales.getRandomColor ();
				}

		}

		void Update ()
		{
				if (!triggeredCoroutine && gameControl.currentState == gameControl.State.GAMEOVER) {
						StartCoroutine ("coloring");
						triggeredCoroutine = true;
				}

				if (triggeredCoroutine && gameControl.currentState == gameControl.State.INGAME) {
						triggeredCoroutine = false;

				}


		}

}
