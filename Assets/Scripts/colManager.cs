﻿using UnityEngine;
using System.Collections;

public class colManager : MonoBehaviour
{

		public ArrayList things;

		public GameObject thing1;
		public GameObject thing2;
		public GameObject thing3;
		public GameObject thing4;
		public GameObject thing5;
		public GameObject thing6;

		public GameObject currentItem;
		public int separation;

		public Vector3 showPos;
		public Quaternion initRot;

		public float spdItem;

	
		public bool movingForward, movingBack = false;
	
	
		void Start ()
		{
				things = new ArrayList ();
				initRot = Quaternion.Euler (new Vector3 (270, 180, 0));

				GameObject th1 = Instantiate (thing1);
				GameObject th2 = Instantiate (thing2);
				GameObject th3 = Instantiate (thing3);
				GameObject th4 = Instantiate (thing4);
				GameObject th5 = Instantiate (thing5);
				GameObject th6 = Instantiate (thing6);


				things.Add (th1);
				things.Add (th2);
				things.Add (th3);
				things.Add (th4);
				things.Add (th5);
				things.Add (th6);


				sortPositions ();
		}

		public void sortPositions ()
		{
				for (int i=0; i< things.Count; i++) {
						GameObject g = things [i] as GameObject;
						g.AddComponent<BoxCollider> ();
						g.AddComponent<movePiece> ();
//						g.AddComponent<MeshRenderer> ();
//						Vector3 size = g.transform.GetComponent<MeshRenderer> ().bounds.size;
						g.transform.rotation = Quaternion.Euler (0, 0, 0);

						print ("COOO: " + (i / 3));
						Vector3 pos = new Vector3 ((i % 3) * 12, (i / 3) * 12, 40);
						g.transform.position = pos;
						g.GetComponent<movePiece> ().backPos = pos;
						g.transform.rotation = initRot;
						g.tag = "item";
				}


		}
	
		// Update is called once per frame
		void Update ()
		{

				if (Input.touchCount > 0) {
						Touch t = Input.GetTouch (0);
						Ray ray = Camera.main.ScreenPointToRay (t.position);

						RaycastHit hit;
						if (Physics.Raycast (ray, out hit, 100)) { 
								if (hit.collider.tag == "item" && !isShowingItem ()) {
										currentItem = hit.collider.gameObject;
										currentItem.GetComponent<movePiece> ().showing = true;
										movingForward = true;
								}
						}

				}
	
				if (currentItem) {
						if (currentItem.GetComponent<movePiece> ().showing) {
								transform.GetChild (1).gameObject.SetActive (false);

								transform.GetChild (0).gameObject.SetActive (currentItem.GetComponent<movePiece> ().showing);
						} else {
								transform.GetChild (1).gameObject.SetActive (false);

								transform.GetChild (0).gameObject.SetActive (currentItem.GetComponent<movePiece> ().showing);
						}
				} else {
						transform.GetChild (0).gameObject.SetActive (false);
						transform.GetChild (1).gameObject.SetActive (true);

				}

		
				if (movingForward && currentItem.transform.position != showPos) {

						currentItem.transform.position = Vector3.Slerp (currentItem.transform.position, showPos, Time.deltaTime * spdItem);

						//checks if finished
						if (currentItem.transform.position.z == showPos.z) {
								currentItem.GetComponent<movePiece> ().showing = true;
								movingForward = false;
						}
				}
		
				if (movingBack) {
			
						currentItem.transform.position = Vector3.Slerp (currentItem.transform.position, currentItem.GetComponent<movePiece> ().backPos, Time.deltaTime * spdItem);
						currentItem.transform.rotation = Quaternion.Slerp (currentItem.transform.rotation, initRot, Time.deltaTime * spdItem);

						//checks if finished
						if (currentItem.transform.position.z == currentItem.GetComponent<movePiece> ().backPos.z) {
								currentItem.GetComponent<movePiece> ().showing = false;
								movingBack = false;
						}
						
				}
		
		
		
		}

		public bool isShowingItem ()
		{

				GameObject[] items = GameObject.FindGameObjectsWithTag ("item");

				bool isShowing = false;

				foreach (GameObject item in items) {
						if (item.GetComponent<movePiece> ().showing) {
								isShowing = true;
								break;
						}
				}
				print ("isSHowing: " + isShowing);
				return isShowing;
		}

	

		public void stopShow ()
		{
				movingBack = true;
				movingForward = false;
		}

		public void changeScene ()
		{
				Application.LoadLevel (2);
		}

}
