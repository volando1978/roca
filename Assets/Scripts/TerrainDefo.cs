﻿using UnityEngine;
using System.Collections;

[RequireComponent (typeof(MeshFilter), typeof(MeshRenderer))]
public class TerrainDefo : MonoBehaviour
{
	private Mesh mesh;

	private void OnEnable ()
	{
		if (mesh == null) {
			mesh = new Mesh ();
			mesh.name = "Surface Mesh";
			GetComponent<MeshFilter> ().mesh = mesh;
		}
		Refresh ();
	}

	public void Refresh ()
	{
	}
}
