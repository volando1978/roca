﻿using UnityEngine;
using System.Collections;

public class Box : MonoBehaviour
{

	public int levelBox;


	public boxDealer boxesDealer;
	public Slots slotsController;



	enum BoxState
	{
		isInGlobo,
		isAscending,
		isDescending,
		isInPiedra
	}

	BoxState currentBoxState;

	Vector3 piedraPos;


	void Start ()
	{
		boxesDealer = GameObject.FindGameObjectWithTag ("BoxDealer").GetComponent<boxDealer> ();
		slotsController = GameObject.FindGameObjectWithTag ("SlotsController").GetComponent<Slots> ();
		currentBoxState = BoxState.isInPiedra;

	}


	void OnTriggerEnter (Collider collider)
	{
		//PIEDRA
		if (collider.gameObject.tag == "Piedra") {
			GetComponent<Rigidbody> ().isKinematic = true;
			currentBoxState = BoxState.isInPiedra;
			if (GetComponentInChildren<MeshRenderer> () != null) {
				GetComponentInChildren<MeshRenderer> ().enabled = true;
			} else {
				print ("NO GENERO MAYA!!!!");
			}


		}

		//BARRA - PLAYER
		if (collider.gameObject.tag == "Player" && currentBoxState == BoxState.isInPiedra) {
			
			if (boxesDealer.CheckIfIsSmaller (gameObject.GetComponent<Box> ().levelBox)) {
				StartCoroutine ("GoToCupula");
				currentBoxState = BoxState.isAscending;
			} else {
//				Vector3 original = transform.position;
//				Quaternion originalRotation = transform.rotation;
//				StartCoroutine ("Shake");
//				transform.position = original;
//				transform.rotation = originalRotation;
			}

//			slotsController.GetComponent<Slots> ().Mete (gameObject);

		}

		//GLOBO
		if (collider.gameObject.tag == "cupula" && currentBoxState == BoxState.isAscending) {
			
			currentBoxState = BoxState.isInGlobo;
			SetCupulaAsParent ();
			gameObject.GetComponent<Collider> ().enabled = false;
			gameObject.GetComponent<Box> ().enabled = true;

			boxesDealer.GrabABox (gameObject);
		}

		//MUERTE BAJA
		if (collider.gameObject.tag == "DeathBox") {

			print ("DEATHBOX: " + GetComponent<Collider> ().gameObject.name);
//			boxesDealer.lostABox (gameObject);
			Destroy (gameObject);
		}	
	}



	IEnumerator GoToCupula ()
	{

		float startX = transform.position.x;
		float startY = transform.position.y;
		float startZ = transform.position.z;

		float startTime = Time.time;
		float overTime = 1f;

		while (currentBoxState != BoxState.isInGlobo) {
			float x = Mathf.Lerp (startX, GetCupulaPos ().x, (Time.time - startTime) / overTime);
			float y = Mathf.Lerp (startY, GetCupulaPos ().y, (Time.time - startTime) / overTime);
			float z = Mathf.Lerp (startZ, GetCupulaPos ().z, (Time.time - startTime) / overTime);
			transform.position = new Vector3 (x, y, z);
			yield return null;
		}

	}

	//TODO:Corregir
	IEnumerator Shake ()
	{
		float secondsLeft = 1;
		float startX = transform.position.x;
		float startY = transform.position.y;
		float startZ = transform.position.z;
		Quaternion originalRotation = transform.rotation;

		float startTime = Time.time;
		float overTime = 1f;

		while (secondsLeft > 0f) {
			secondsLeft -= Time.deltaTime;
			float r = Random.Range (20, -20);
			float x = Mathf.Lerp (startX, startX + r, (Time.time - startTime) / overTime);
			float y = Mathf.Lerp (startY, startY + r, (Time.time - startTime) / overTime);
			float z = Mathf.Lerp (startZ, startZ + r, (Time.time - startTime) / overTime);
//			transform.position = new Vector3 (x, y, z);
			transform.rotation = Quaternion.Euler (x + r, y + r, z + r);
			yield return null;
		}
		Vector3 pos = new Vector3 (startX, startY, startZ);
		transform.position = pos;
		transform.rotation = originalRotation;

//		Debug.Log ("Countdown finished!");
	}

	void SetCupulaAsParent ()
	{
		transform.SetParent (GetCupula ().transform.GetChild (0).transform, true);
	}

	Vector3 GetCupulaPos ()
	{
		return GetCupula ().GetComponent<BoxCollider> ().transform.position;
	}

	GameObject GetCupula ()
	{
		GameObject c = GameObject.FindGameObjectWithTag ("cupula");
		return c;
	}

	public void SetBackToPiedra ()
	{
		currentBoxState = BoxState.isDescending;
//		GetComponent<Box> ().isInGlobo = false;
		GetComponent<Collider> ().enabled = true;
		GetComponent<Rigidbody> ().isKinematic = false;
		GetComponent<Box> ().enabled = true;

		boxesDealer.lostABox (gameObject);

		StartCoroutine ("MoveToPiedra");
	}


	IEnumerator MoveToPiedra ()
	{

		Vector3 start = transform.position;
		float startTime = Time.time;
		float overTime = 1f;
				
		Vector3 pos = new Vector3 (transform.position.x, piedraPos.y, transform.position.z);
		while (transform.position.y <= piedraPos.y) {
			transform.position = Vector3.MoveTowards (start, pos, (Time.time - startTime) / overTime);
			yield return new WaitForEndOfFrame ();
		}

		yield return null;

	}

}
