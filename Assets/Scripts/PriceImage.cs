﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;


public class PriceImage : MonoBehaviour
{

	public List<Sprite> spriteList = new List<Sprite> ();
	private Image pic;
	int rotationFactor = 0;


	public void showPopUpPrice ()
	{
		rotationFactor = 0;

		pic = GetComponent<Image> ();
//				pic.sprite = spriteList [(int)globales.level - 1];
		StartCoroutine (rotateGem ());
	}

	IEnumerator rotateGem ()
	{

//				int n = 0;
		Vector2 r = new Vector2 ();
		while (rotationFactor < 1080) {
			r.y = rotationFactor;
			r.x = transform.rotation.x;
//						pic.transform.rotation = Quaternion.Euler (r);
			pic.rectTransform.rotation = Quaternion.Euler (r);
//						n++;
			rotationFactor += 30;
//						rotationFactor = (int)Mathf.Lerp ((float)rotationFactor, (float)rotationFactor + n, 0.01f * Time.deltaTime);
			yield return null;

		}
	}


	Image Pic { get { return pic; } set { pic = value; } }



}
