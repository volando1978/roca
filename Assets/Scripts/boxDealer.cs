﻿using UnityEngine;
using System.Collections.Generic;


public class boxDealer : MonoBehaviour
{

	public GameObject Box;
	public List<GameObject> GameBoxes;
	public List<GameObject> PlayerBoxes;
	GameObject thing;
	public GameObject Arrow;

	public GameObject[] allThings;
	public Color[] colores;
	public Shader shaderV;

	public bool generating = false;
	public int numDiversidad;
	public float helperPosition;


	gameControl gameController;


	void Start ()
	{
		shaderV = Shader.Find ("Vertex Colored");
		PlayerBoxes = new List<GameObject> ();
		GameBoxes = new List<GameObject> ();
		gameController = GameObject.FindGameObjectWithTag ("GameController").GetComponent<gameControl> ();


		CleanArea ();
		GenerateBoxes ();

	}

	void CleanArea ()
	{
		
		GameObject[] boxes = GameObject.FindGameObjectsWithTag ("Box");

		foreach (GameObject g in boxes) {
			Destroy (g);
		}

	}

	Vector3[] GetPointsOnSphere (int nPoints)
	{
		float fPoints = (float)nPoints;

		Vector3[] points = new Vector3[nPoints];

		float inc = Mathf.PI * (3 - Mathf.Sqrt (5));
		float off = 2 / fPoints;

		for (int k = 0; k < nPoints; k++) {
			float y = k * off - 1 + (off / 2);
			float r = Mathf.Sqrt (1 - y * y);
			float phi = k * inc;

			points [k] = new Vector3 (Mathf.Cos (phi) * r, y, Mathf.Sin (phi) * r);
		}

		return points;
	}

	void GenerateBoxes ()
	{
		GrowGenerator ();
		generating = true;

		Vector3[] myPoints = GetPointsOnSphere (gameController.numBoxes - gameController.level);

		foreach (Vector3 point in myPoints) {
//			float scale = gameObject.GetComponent<SphereCollider> ().radius * 2 * 120;
			float scale = gameObject.transform.localScale.x / 2;

			Vector3 normal = ((point * scale) - transform.position);
			normal = Vector3.Cross (normal.normalized, Vector3.left);
			Quaternion rotation = Quaternion.LookRotation (normal);
			print (rotation);
			GameObject box = Instantiate (Box, point * scale, rotation) as GameObject;
			box.SetActive (false);

			AssignLevel ();
			AssignShape (box);
			box.name = "BOX " + box.GetComponent<Box> ().levelBox;
			GameBoxes.Add (box);
			box.SetActive (true);
		}

		CleanSort ();
		SetRemainingBoxes ();
		CheckBoxesLeft ();
		gameController.DisplayBoxesLeft ();

		generating = false;

	}

	void SetRemainingBoxes ()
	{

		gameController.remainingBoxes = 0;

		for (int m = 0; m < GameBoxes.Count; m++) {
			if (GameBoxes [m].GetComponent<Box> ().levelBox == gameController.level) {
				gameController.remainingBoxes += 1;
			}
		}

	}

	private GameObject CreateBox (Vector3 pos)
	{
//		Vector3 pos = GenRandomPos ();
		GameObject box = Instantiate (Box, pos, Quaternion.identity) as GameObject;
		box.SetActive (false);

		return box;
	}

	private void AssignLevel ()
	{
		Box.GetComponent<Box> ().levelBox = Random.Range (gameController.level, gameController.level + numDiversidad);
	}


	void Shaping (int index, GameObject box)
	{

		thing = Instantiate (allThings [index])as GameObject;


		thing.transform.SetParent (box.transform, true);

		thing.GetComponent<Renderer> ().material.shader = shaderV;
		thing.GetComponent<Renderer> ().material.color = colores [index];

		thing.transform.localScale = new Vector3 (10, 10, 10);
//		thing.transform.rotation = Quaternion.Euler (0, 0, 0);

		thing.transform.position = box.transform.position;
		thing.transform.rotation = box.transform.rotation;

//		thing.transform.rotation = Quaternion.Euler (Vector3.zero);


		thing.GetComponentInChildren<MeshRenderer> ().enabled = true;
		print ("INDEX DE SHAPING: " + index);
	}

	public void AssignShape (GameObject box)
	{

		switch (box.GetComponent<Box> ().levelBox) {

		case 1:
			Shaping (1, box);
			break;
		case 2:
			Shaping (2, box);
			break;
		case 3:
			Shaping (3, box);

			break;
		case 4:
			Shaping (4, box);

			break;
		case 5:
			Shaping (5, box);

			break;
		case 6:
			Shaping (6, box);

			break;
		case 7:
			Shaping (7, box);

			break;

		}

	}

	void resetGatheredBoxes ()
	{
		gameController.resetGatheredAndRemainingBoxes ();
	}
		
	// PUBLIC

	public void GrabABox (GameObject boxGathered)
	{

		AddBoxToPlayerBoxes (boxGathered);
		RemoveFromGameBoxes (boxGathered);
		gameController.IncrementGatheredBox ();

//		if (!generating)
		CleanSort ();
		CheckBoxesLeft ();

	}

	public void lostABox (GameObject boxLost)
	{
		RemoveFromGameBoxes (boxLost);
	}

	private void AddBoxToPlayerBoxes (GameObject box)
	{
		PlayerBoxes.Add (box);

	}

	public void RemoveFromGameBoxes (GameObject box)
	{
		if (GameBoxes.Count > 0) {
			GameBoxes.Remove (box);

		}
//		gameController.DisplayFaces ();
	}


	public void CleanSort ()
	{
		//CLEAN
		for (int c = GameBoxes.Count - 1; c > -1; c--) {
			if (GameBoxes [c] == null)
				GameBoxes.RemoveAt (c);
		}


		//SORT LEVEL

		for (int i = 0; i < GameBoxes.Count; i++) {
			for (int j = i + 1; j < GameBoxes.Count; j++) {

				if (GameBoxes [i].GetComponent<Box> ().levelBox > GameBoxes [j].GetComponent<Box> ().levelBox) {

					GameObject aux = GameBoxes [i] as GameObject;
					GameBoxes [i] = GameBoxes [j];
					GameBoxes [j] = aux;

				}

			}
		}

		print ("LIST SORTED");
	}

	void CheckBoxesLeft ()
	{
		CleanSort ();

		if (GameBoxes.Count == 0) {
			NextLevel ();
		}


		//CHECK IF there is no more of the same level
		print ("-------------------");

		for (int m = 0; m < GameBoxes.Count; m++) {

			print ("CHECK part " + GameBoxes [m].name);

			if (GameBoxes [m].GetComponent<Box> ().levelBox <= gameController.level) {
				DrawHelper ();
				return;
			} else {
				NextLevel ();
				CheckBoxesLeft ();
				gameController.DisplayBoxesLeft ();

			}
		}
	}

	void DrawHelper ()
	{

		int boxesLeft = (gameController.remainingBoxes - gameController.gatheredBoxes);
		print ("REMANING in HELPER " + boxesLeft);

		if (boxesLeft == 1) {
			Transform t = GameBoxes [0].transform;
			Vector3 indicatorPos = new Vector3 (t.position.x, t.position.y + helperPosition, t.position.z);
			Instantiate (Arrow, indicatorPos, Quaternion.identity);
		}
	}

	void DeleteHelper ()
	{
		
		Destroy (GameObject.FindGameObjectWithTag ("Indicator"));
	}

	void GrowGenerator ()
	{
		
//		Vector3 sc = new Vector3 (25 * gameController.level, 25 * gameController.level, 25 * gameController.level);
//		transform.localScale = transform.localScale + sc;
	}

	void NextLevel ()
	{
		DeleteHelper ();
		gameController.LevelUp ();
		gameController.AddColorToPipe (colores [gameController.level]);
		gameController.PurgeGlobe ();
		print ("LEVEL UP: " + gameController.level);
		GrowGenerator ();
		if (!generating)
			GenerateBoxes ();
		return;
	}

	public bool CheckIfIsSmaller (int boxLevel)
	{
//		bool isSmaller = false;

		if (boxLevel <= gameController.level) {
			return true;
		} else {
			return false;
		}
	}

	//PRIVATE

	private Vector3 GenRandomPos ()
	{
//		for (int x = 0; x < transform.localScale.x; x + 10) {
//			for (int z = 0; z < transform.localScale.z; z + 10) {
//				randomX =
//				randomY =
//			}
//		}

		float randomX = Random.Range ((transform.position.x - transform.localScale.x / 2), (transform.position.x + transform.localScale.x / 2));
		float randomZ = Random.Range ((transform.position.z - transform.localScale.z / 2), (transform.position.z + transform.localScale.z / 2));

		return new Vector3 (randomX, transform.position.y, randomZ);
	}
}
