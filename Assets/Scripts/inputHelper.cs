﻿using UnityEngine;
using System.Collections;

public class InputHelper : MonoBehaviour
{
	
	
	
		public static bool left ()
		{
#if UNITY_EDITOR
				if (Input.GetKey ("left")) {
						return true;
				}
#endif


				return false;
		}
	
		public static bool right ()
		{
				#if UNITY_EDITOR

				if (Input.GetKey ("right")) {
						return true;
				}
				#endif

				return false;
		
		}
	
		public static bool up ()
		{
				#if UNITY_EDITOR

				if (Input.GetKey ("up")) {
						return true;
				}
				#endif

				return false;
		
		}
	
		public static bool down ()
		{
				#if UNITY_EDITOR

				if (Input.GetKey ("down")) {
						return true;
				}
				#endif

				return false;
		
		}
	
		public static bool leftDown ()
		{
				#if UNITY_EDITOR

				if (Input.GetKeyDown ("left")) {
						return true;
				}
				#endif

				return false;
		}
	
		public static bool rightDown ()
		{
				#if UNITY_EDITOR

				if (Input.GetKeyDown ("right")) {
						return true;
				}
				#endif
				#if UNITY_IOS || UNITY_ANDROID
//		if ()


				#endif


				return false;
		
		}
	
		public static bool upDown ()
		{
				#if UNITY_EDITOR

				if (Input.GetKeyDown ("up")) {
						return true;
				}
				#endif

				return false;
		
		}
	
		public static bool downDown ()
		{
				#if UNITY_EDITOR

				if (Input.GetKeyDown ("down")) {
						return true;
				}
				#endif

				return false;
		
		}
	
	
		public static bool space ()
		{
				if (Input.GetKeyDown ("space")) {
						return true;
				}

				return false;
		}
	
		public static bool 	mouseClick ()
		{
				if (Input.GetButtonDown ("Fire1")) {
						return true;
				}

				return false;
		}
	
	
	
		//				if (Input.acceleration.x < 0.05) {
		//						transform.position += new Vector3 (-speed, 0, 0);
		//				}
		//				if (Input.acceleration.x > -0.05) {
		//						transform.position += new Vector3 (speed, 0, 0);
		//				}
		//				if (Input.acceleration.y < 0.05) {
		//						transform.position += new Vector3 (0, -speed, 0);
		//				}
		//				if (Input.acceleration.y > -0.05) {
		//						transform.position += new Vector3 (0, speed, 0);
		//				}
	
		public static Vector2 touch ()
		{
				Vector2 touchDeltaPosition = Vector2.zero;
				if (Input.touchCount > 0 && 
						Input.GetTouch (0).phase == TouchPhase.Moved) {
			
						// Get movement of the finger since last frame
						touchDeltaPosition = Input.GetTouch (0).deltaPosition;
			
						// Move object across XY plane
						//			transform.Translate (-touchDeltaPosition.x * speed, 
						//			                     -touchDeltaPosition.y * speed, 0);
			
						//						return touchDeltaPosition;
			
				}
				return touchDeltaPosition;
		}

		public static Vector3 touchPos ()
		{
				Vector3 touchDeltaPosition = Vector2.zero;
				if (Input.touchCount > 0 && 
						Input.GetTouch (0).phase == TouchPhase.Began) {
			
						// Get movement of the finger since last frame
						touchDeltaPosition = Camera.main.ScreenToWorldPoint (Input.GetTouch (0).position);

			
				}
				return touchDeltaPosition;
		}
	
	
}




//				transform.Translate (Input.acceleration.x * 0.5f, Input.acceleration.y * 0.5f, transform.position.z);








