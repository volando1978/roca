using UnityEngine;
using System.Collections;

//using GooglePlayGames;
using System.Collections.Generic;

using UnityEngine.SocialPlatforms.GameCenter;
using UnityEngine.Advertisements;

using UnityEngine.SocialPlatforms;


// PROTOSKI GAME CONTROL


public class gameControl : MonoBehaviour
{

	[Range (200, 1600)]
	public float
		offsetToBarReappear;
	
	//--STATES
	public enum State
	{
		MENU = 0,
		INGAME = 1,
		GAMEOVER = 2,
		WORLDMENU = 3,
		SELECTED = 4,
		PAUSE = 5,
		SETTINGS = 6,
		CONTINUE = 7,
		LEVELUP = 8,
		TUTORIAL = 9,
		RESET = 10

	}

	enum MDirection
	{
		up = 0,
		down = 1,
		left = 2,
		right = 3
	}

	MDirection currentMDirection;

	public State statusTally;
	[SerializeField]
	public static State
		currentState;
	public State previousState;

	public GameObject Gen;
	GameObject currentGen;

	//	public GameObject player;
	//	GameObject currentPlayer;

	public GameObject Globo;
	public GameObject currentGlobo;

	public float speedXGlobo = 300f;
	public float speedYGlobo = 400f;
	public float valueCam;


	public GameObject cameraTarget;

	public GameObject terrain;
	GameObject currentTerrain;

	public GameObject Piedra;
	GameObject CurrentPiedra;
	public Mesh[] Piedras;

	public GameObject music;
	GameObject currentMusic;

	GameObject currentlights;
	public GameObject HUD;

	public GameObject Timer;

	Botones headUpDisplay;
	public GameObject stageNameObj;
	public GameObject sameBoxesLeftHUD;


	public GameObject metricsTotalMeters;
	public GameObject medals;
	public GameObject Faces;
	public GameObject swapLayout;
	public GameObject DropButton;
	public GameObject ButtonMovementSwitch;
	public GameObject SlotsController;
	ButtonSwitcher buttonSwitcher;

	public GameObject holePrefab;
	GameObject currentHole;

	public int gatheredBoxes { get; set; }
	//	public int sameLevelBoxes { get; set; }
	public int remainingBoxes = 0;
	public int numBoxes;
	public int level;
	public int setBoxes = 0;
	public float timer = 60;
	public int pipeIncrease;
	public int ClampIncrease;

	public int initZoom;
	public int finalZoom;
	public int finalZoomInGame;
	public float speedZoom;
	public float speedZoomStepBack;
	public float cameraStep;
	public float heightPiedra;
	public float alturaGen;
	public float alturaGlobo;

	float zoom;
	float globoYPos;
	public float globeLiftStep;
	Quaternion initCamRot;

	bool camMoving = false;
	bool runningZoomIn = false;
	bool runningZoomOut = false;
	bool isGloboResettingPos = false;


	bool isControllingMountain = false;

	#if UNITY_IOS
	GameCenterSingleton gameCenter;
	#endif

	void Awake ()
	{
		#if UNITY_IOS

		gameCenter = new GameCenterSingleton ();
		gameCenter.Initialize ();
		Advertisement.Initialize ("0000", false);
		#endif

		#if UNITY_ANDROID

//		Advertisement.Initialize ("55854", false);
		// recommended for debugging:
//				PlayGamesPlatform.DebugLogEnabled = true;
		// Activate the Google Play Games platform
//		PlayGamesPlatform.Activate ();
//		Social.localUser.Authenticate ((bool success) => {
//			if (success) {
//				Debug.Log ("Login Sucess");
//			} else {
//				Debug.Log ("Login failed");
//			}
//		});
		#endif

		headUpDisplay = HUD.GetComponentInChildren <Botones> ();

		buttonSwitcher = ButtonMovementSwitch.GetComponent<ButtonSwitcher> ();
	}

	void Start ()
	{
		
		currentState = State.MENU;
		UpdateHUD ();

		currentMusic = Instantiate (music);
		GetComponent<globales> ().soundCheck ();
		setLevel ();

		Vector3 cameraTargetPos = new Vector3 (cameraTarget.transform.position.x, alturaGlobo, cameraTarget.transform.position.z);
		cameraTarget.transform.position = cameraTargetPos;
		initCamRot = cameraTarget.transform.rotation;
		Camera.main.fieldOfView = initZoom;
		zoom = finalZoom + cameraStep;
		globoYPos = globeLiftStep;
	}

	private void setLevel ()
	{
		level = 1;
		setBoxes = 0;
		print ("LEVEL: " + level);
	}

	public void toGame () // new game
	{
		StartCoroutine ("CameraZoom");
		cameraTarget.transform.rotation = initCamRot;
		timer = 60;
		level = 1;

		globales.isReseted = false;
		currentState = State.INGAME;
		setGameNumber ();
		resetContinueFlag ();
		globales.showNewRecord = false;
		UpdateHUD ();


		setMusic ();
		SoundManager.playStartClip ();

//		stageNameObj.SetActive (true);
//		stageNameObj.GetComponent<StageName> ().setStageName ();
//		UpdateHUD ();

		resetGatheredAndRemainingBoxes ();
		CreateGlobo ();
//		CreatePiedra ();
		CreateGen ();
//		IncreasePiedraDiameter ();

		//TALLY
//		SlotsController.GetComponent<Slots> ().StartTally ();
		SlotsController.GetComponent<Slots> ().SetTally ();
		DisplayBoxesLeft ();


	}


	IEnumerator CameraZoom ()
	{
		float start = Camera.main.fieldOfView;
		float startTime = Time.time;
		float overTime = 1f;

		while (Camera.main.fieldOfView > finalZoom) {
			Camera.main.fieldOfView = Mathf.Lerp (start, finalZoom, (Time.time - startTime) / overTime);
			yield return new WaitForSeconds (0);

//			print ("Coroutine CameraZoom gamecontrol");

			if (Camera.main.fieldOfView <= finalZoom) {
				StopCoroutine ("CameraZoom");
			}
		}
		yield return new WaitForSeconds (0);

	}

	IEnumerator LiftGlobe ()
	{
		
		globoYPos += globeLiftStep;
		float start = currentGlobo.transform.position.y;
		float startTime = Time.time;
		float overTime = 1f;

		while (currentGlobo.transform.position.y < globoYPos) {
			float yValue = Mathf.Lerp (start, globoYPos, (Time.time - startTime) / overTime);
			Vector3 pos = new Vector3 (currentGlobo.transform.position.x, yValue, currentGlobo.transform.position.z);
			currentGlobo.transform.position = pos;

			yield return null;
		}
	}

	IEnumerator CameraStepBack ()
	{
		zoom += cameraStep;
		float start = Camera.main.fieldOfView;
		float startTime = Time.time;
		float overTime = 1f;

		while (Camera.main.fieldOfView < zoom) {
			Camera.main.fieldOfView = Mathf.Lerp (start, zoom, (Time.time - startTime) / overTime);
			yield return new WaitForSeconds (0);

			if (Camera.main.fieldOfView >= zoom) {
				
				StopCoroutine ("CameraStepBack");
			}
		}
		yield return new WaitForSeconds (0);

	}


	//---------GAME-------------

	public void LevelUp ()
	{
		resetGatheredAndRemainingBoxes ();
		level = level + 1;
		IncreaseGameTime ();
		SlotsController.GetComponent<Slots> ().SetTally ();
		DisplayBoxesLeft ();
		IncreasePlayerPipe ();

		//TODO: FEEDBACK LEVEL UP
		//TODO: funcion recorre Timer HIGHLIGHT
		return;
	}

	void IncreasePiedraDiameter ()
	{
//			Vector3 sumaSc = new Vector3 (100, 0, 100);
//			CurrentPiedra.transform.localScale = currentGen.transform.localScale + sumaSc;
	
	}

	void IncreasePlayerPipe ()
	{
//		print ("O  NOME cc " + currentGlobo.transform.childCount);
//
//		print ("O  NOME " + currentGlobo.transform.GetChild (1).gameObject.name);
//
//		Vector3 pipe = new Vector3 (currentGlobo.transform.GetChild (1).transform.localScale.x + pipeIncrease,
//			               currentGlobo.transform.GetChild (1).transform.localScale.y,
//			               currentGlobo.transform.GetChild (1).transform.localScale.z + pipeIncrease);
//		currentGlobo.transform.GetChild (1).GetComponent<BoxCollider> ().transform.localScale = pipe;
//
//		GetComponent<Controles> ().ClampRestriction = GetComponent<Controles> ().ClampRestriction + ClampIncrease;
//		GetComponent<Controles> ().ClampRestrictionY = GetComponent<Controles> ().ClampRestrictionY + ClampIncrease;
	}

	public void AddColorToPipe (Color c)
	{
//		print ("color " + c);
//		c.a = 50;
//		currentGlobo.transform.GetChild (1).GetComponent<Renderer> ().material.color = c;
	}

	void IncreaseGameTime ()
	{

		StartCoroutine ("MoreTime");
//		timer += 30f;
		print ("TOP:");
	}

	IEnumerator MoreTime ()
	{
		float top = timer + 30f;
		print ("TOP: ------------" + top);
		while (timer < top) {

			timer += 0.01f / Time.deltaTime;
			Timer.GetComponent<TimerHUD> ().DisplayTimer ();
			yield return null;
		}


	}

	public void DisplayBoxesLeft ()
	{
		sameBoxesLeftHUD.GetComponent<MESSAGE_UI> ().DisplayBoxesLeft ();

	}

	public void PurgeGlobe ()
	{
		currentGlobo.GetComponent<Cupula> ().Purge ();
		print ("JUST PURGE");
		SlotsController.GetComponent<Slots> ().SetTally ();
		//		stageNameObj.GetComponent<StageName> ().setStageName ();
		UpdateHUD ();
	}

	void ChangeBoxesSet ()
	{
		setBoxes += 1;
	}

	void ResetBoxesSet ()
	{
		setBoxes = 0;
	}

	public void resetGatheredAndRemainingBoxes ()
	{
		gatheredBoxes = 0;
		remainingBoxes = 0;
	}

	public void IncrementGatheredBox ()
	{
		gatheredBoxes += 1;
//		StartCoroutine ("CameraStepBack");
//		StartCoroutine ("LiftGlobe");
		DisplayBoxesLeft ();
		UpdateHUD ();

	}



	void CreateGen ()
	{
		
		if (!currentGen) {
			currentGen = (GameObject)Instantiate (Gen, Vector3.zero, Quaternion.identity);
			currentGen.transform.position = new Vector3 (0, 0, 0);
		} else {
			Destroy (currentGen);
			currentGen = (GameObject)Instantiate (Gen, Vector3.zero, Quaternion.identity);
			currentGen.transform.position = new Vector3 (0, 0, 0);

		}

	}


	void CreateGlobo ()
	{
		if (!currentGlobo) {
			currentGlobo = (GameObject)Instantiate (Globo, Vector3.zero, Quaternion.identity);
			currentGlobo.transform.position = new Vector3 (0, alturaGlobo, 0);
//			cameraTarget.transform.SetParent (currentGlobo.transform);

		} else {
			Destroy (currentGlobo);
			currentGlobo = (GameObject)Instantiate (Globo, Vector3.zero, Quaternion.identity);
			currentGlobo.transform.position = new Vector3 (0, alturaGlobo, 0);


		}
		ResetRotationGlobo ();
	}

	void ResetRotationGlobo ()
	{
		Vector3 pos = new Vector3 (0, Camera.main.transform.rotation.y, 0);
		currentGlobo.transform.rotation = Quaternion.Euler (pos);

	}

	void CreatePiedra ()
	{
		if (!CurrentPiedra) {
			CurrentPiedra = (GameObject)Instantiate (Piedra, Vector3.zero, Quaternion.identity);
			CurrentPiedra.transform.position = new Vector3 (0, heightPiedra, 0);
//			CurrentPiedra.transform.SetParent (currentGlobo.transform);
		} else {

			Destroy (CurrentPiedra);
			CurrentPiedra = (GameObject)Instantiate (Piedra, Vector3.zero, Quaternion.identity);
			CurrentPiedra.transform.position = new Vector3 (0, heightPiedra, 0);

		}
	}

	public  void callScoreTable ()
	{
		#if UNITY_IOS
		gameCenter.ShowLeaderboardUI ();
		#endif

		#if UNITY_ANDROID



		((PlayGamesPlatform)Social.Active).ShowLeaderboardUI ("CgkI3qqFwo8fEAIQAQ");
		#endif
	}



	void Update ()
	{

		/*/Timer*/
		if (currentState == State.INGAME) {
			timer -= Time.deltaTime;
			Timer.GetComponent<TimerHUD> ().DisplayTimer ();
			if (timer <= 0) {
				finishGame ();
			}
		}

		statusTally = currentState;
	}


	IEnumerator ResetPosGlobo ()
	{
		isGloboResettingPos = true;
		while (currentGlobo.transform.position != Vector3.zero) {

			currentGlobo.transform.position = Vector3.Lerp (currentGlobo.transform.position, Vector3.zero, speedXGlobo / 50 * Time.deltaTime);

			if (currentGlobo.transform.position == Vector3.zero) {
				StopCoroutine ("ResetPosGlobo");
				isGloboResettingPos = false;

			}
			yield return null;
		}
		yield return null;
	}

	void PlayTransicion ()
	{
		Camera.main.GetComponent<Transicion> ().FillCam ();
	}

	void createHole ()
	{
		if (!currentHole) {
			Vector3 pos = new Vector3 (globales.coordContinue.x, 40, globales.coordContinue.z - 500);
			currentHole = Instantiate (holePrefab, pos, Quaternion.identity)as GameObject;
			currentHole.transform.SetParent (Camera.main.transform);
		} 
	}

	void deleteHole ()
	{
		if (currentHole) {
			Vector3 continueInPlayerPos = new Vector3 (globales.coordContinue.x, 0, globales.coordContinue.z);
			currentHole.transform.position = continueInPlayerPos;
			currentHole.GetComponent<GrowSphere> ().goShrink ();
		}
	}

	void ShowMetrics ()
	{
		UpdateHUD ();
		metricsTotalMeters.SetActive (true);
		metricsTotalMeters.GetComponent<totalMetersText> ().showMetricsEndGame ();

	}

	public void endGameGoToScores ()
	{
//				currentState = gameControl.State.GAMEOVER;
//				UpdateHUD();
//				setScores ();
		globales.isContinue = false;
		finishGame ();

	}


	public void setScores ()
	{
		#if UNITY_IOS
		if (globales.meterScore > globales.maxDistance) {
			globales.maxDistance = globales.meterScore;
			
			globales.showNewRecord = true;
			
			if (Social.localUser.authenticated) {
				gameCenter.ReportScore ((long)globales.maxDistance, "BestRun");
			}
		}
		
		if (globales.gameNight == true) {
			if (globales.meterScore > globales.maxDistanceNight) {
				globales.maxDistanceNight = globales.meterScore;
				print ("nightScore " + globales.maxDistanceNight);
				globales.showNewRecordNight = true;
				
				if (Social.localUser.authenticated) {
					gameCenter.ReportScore ((long)globales.maxDistanceNight, "BestRun");
				}
			}
		}

		print (globales.totalLifeMeters);
		globales.totalLifeMeters += globales.meterScore;

		gameCenter.ReportScore ((long)globales.totalLifeMeters, "MarathonScore");
		
		#endif

		#if UNITY_ANDROID

		if (globales.scorePoints > globales.maxDistance) {
			globales.maxDistance = globales.scorePoints;
			globales.showNewRecord = true;
			
			Social.ReportScore (globales.maxDistance, "CgkI3qqFwo8fEAIQAg", (bool success) => {
				// handle success or failure
				if (success) {
					Debug.Log ("U_pdate_ Score Success");

				} else {
					Debug.Log ("U_pdate_ Score Fail");
				}
			});

//						PlayGamesPlatform.Instance.ReportScore (globales.maxDistance, "CgkI_fuagIsJEAIQBg", (bool success) => {
//								// handle success or failure
//						});
		}

		globales.totalLifeMeters += globales.scorePoints;

//		Social.ReportScore (globales.totalLifeMeters, "CgkI3qqFwo8fEAIQAQ", (bool success) => {
		// handle success or failure
//		});

		#endif

		ShowMetrics ();
		
	}

	void setGameNumber ()
	{
		
		globales.numberOfGames += 1;
		print ("NUMBER OF GAMES " + globales.numberOfGames);
	}


	void setMusic ()
	{
		Destroy (currentMusic);
		currentMusic = Instantiate (music);
		SoundManager.StopAllSounds ();
		currentMusic.GetComponent<AudioSource> ().mute = globales.musicSwitch;
	}

	void resetContinueFlag ()
	{
		if (!globales.isContinue) {
			globales.isContinue = true;
		} 
	}



	public void showAdToContinue ()
	{
		#region ADS ACTIVATE TO RETAIL
		// UNITY ADS VIDEO
		// Show with default zone, pause engine and print result to debug log
		Advertisement.Show (null, new ShowOptions {
//			pause = false,
			resultCallback = result => {
				if (result == ShowResult.Finished) {
					continueGameCountDown ();
				}
			}
		});
		#endregion
		continueGameCountDown ();
	}




	public  void continueGameCountDown ()
	{
	
		globales.dead = false;
		currentState = State.INGAME;
		UpdateHUD ();
//		Vector3 barPos = new Vector3 (player.transform.position.x, player.transform.position.y, player.transform.position.z - offsetToBarReappear);
		globales.isContinue = false;
		globales.passStage = false;
	
		deleteHole ();
		SoundManager.playStartClip ();
	
		//RESETS
		globales.showNewRecord = false;
		globales.showNewRecordNight = false;
	
		if (!currentGlobo) {
			if (globales.playerFelt) {
				Vector3 continueInCenter = new Vector3 (0, 0, globales.coordContinue.z);
				currentGlobo = (GameObject)Instantiate (Globo, continueInCenter, Quaternion.identity);
	
			} else {
				Vector3 continuePos = new Vector3 (globales.coordContinue.x, 0, globales.coordContinue.z);
				currentGlobo = (GameObject)Instantiate (Globo, continuePos, Quaternion.identity);
			}
		}
	}

	public void finishGame ()
	{

		PlayTransicion ();
		UpdateHUD ();

		Destroy (currentGen);

		if (currentState == State.INGAME) {
			SoundManager.playEndGameClip ();
		}

		if (globales.isContinue) {
			currentState = gameControl.State.CONTINUE;
			UpdateHUD ();
		} else {
			currentState = gameControl.State.GAMEOVER;
			UpdateHUD ();
			setScores ();
			toGameOverRoomSetDataAndAds (); //set data and delete hole
		}
	}

	
	IEnumerator waitForTouchDown ()
	{
		while (Input.touchCount < 1) {
			yield return null;
		}
	}

	IEnumerator waitTime (float seconds)
	{
		var t = 0;
		while (t < seconds) {
			t = t + 1;
			yield return new WaitForSeconds (2f);
		}
		yield return null;
	}

	
	public void setLastKills ()
	{
		globales.lastGamePoints = globales.scorePoints;
	}


	// INSTANTIATE STATES
	public void toMenu ()
	{
		globales.dead = false;
		currentState = State.MENU;
		UpdateHUD ();
		deleteHole ();


		SoundManager.StopAllSounds ();
		if (!currentMusic) {
			currentMusic = Instantiate (music);
		} 
		setMusic ();
	}

	public void destroyProps ()
	{
		GameObject[] gos = GameObject.FindGameObjectsWithTag ("Rock");
		foreach (GameObject go in gos) {
			Destroy (go);
		}
	}


	public void toGameOverRoomSetDataAndAds ()
	{
		globales.setData ();
		deleteHole ();

		#region ADS
//				// ADS SHOWING
//				if (!globales.goldenVersion && globales.numberOfGames > 3) {
//
//						#if UNITY_IPHONE
//			
//						bool isPad = false;
//						if ((UnityEngine.iOS.Device.generation.ToString ()).IndexOf ("iPad") > -1) {
//
//								print (" AdInterstitial  loaded " + ADInterstitial.fullscreenAd.loaded + "  show apple " + globales.showAdApple + " show unity " + globales.showUnity);
//
//								isPad = true;
//								//IAD
//								if (ADInterstitial.fullscreenAd.loaded && globales.showAdApple && globales.numberOfGames % 3 == 0) {
//										ADInterstitial.fullscreenAd.Show ();
//										globales.showAdApple = false;
//								}
//
//								//UNITYAD
//								if (!ADInterstitial.fullscreenAd.loaded && globales.numberOfGames % 3 == 0) {
//										if (Advertisement.isReady () && !globales.showUnity && currentState == State.GAMEOVER) {
//						
//												Advertisement.Show ();
//												globales.showUnity = true;
//												globales.showAdApple = false;
//										}
//								}
//						}
//				
//						if (!isPad && globales.numberOfGames % 3 == 0) {
//								if (Advertisement.isReady () && !globales.showUnity && currentState == State.GAMEOVER) {
//					
//										Advertisement.Show ();
//										globales.showUnity = true;
//								}
//
//								
//						}
//
//						#endif
//
//						#if UNITY_ANDROID
//
//						//UNITYAD
//						if (globales.numberOfGames % 3 == 0) {
//								if (Advertisement.isReady () && !globales.showUnity && currentState == State.GAMEOVER) {
//						
//										Advertisement.Show ();
//										globales.showUnity = true;
////												globales.showAdApple = false;
//								}
//						}
//						
//			
//						#endif
//				}
		#endregion
		
	}

	public void DisplayMedals ()
	{
		medals.GetComponent<HudPics> ().showMedals ();
	}

	public void DisplayFaces ()
	{
		Faces.GetComponent<FacesHud> ().showFaces ();
	}


	public void toSettings ()
	{
		previousState = currentState;
		currentState = State.SETTINGS;
		UpdateHUD ();
	}

	public void toReset ()
	{
		previousState = currentState;
		currentState = State.RESET;
		UpdateHUD ();
	}


	public void pauseGame ()
	{
		currentState = State.PAUSE;
		UpdateHUD ();
		Time.timeScale = 0;			
	}

	public void backToGame ()
	{
		currentState = State.INGAME;
		UpdateHUD ();
		Time.timeScale = 1f;	
	}

	public void restartGame ()
	{
		toGame ();
	}


	public void BackButton ()
	{
		if (currentGlobo != null) {
			pauseGame ();
		} 

		if (currentState == State.RESET) {
			currentState = State.MENU;
		}
				
		if (previousState == State.MENU) {
			medals.GetComponent<HudPics> ().showMedals ();
		}
		currentState = previousState;
		print ("aqui");
		UpdateHUD ();
	}

	public void BackButtonToSettings ()
	{
		toSettings ();
		previousState = State.MENU;
		UpdateHUD ();
	}


	public void UpdateHUD ()
	{
		headUpDisplay.updateHUD ();
	}

	
	public void OnApplicationQuit ()
	{
		Destroy (gameObject);
	}
}


//IEnumerator CameraZoomOut ()
//{
//
//	float start = Camera.main.fieldOfView;
//	float startTime = Time.time;
//	float overTime = 1f;
//
//	while (Camera.main.fieldOfView < initZoom) {
//		Camera.main.fieldOfView = Mathf.Lerp (start, initZoom, (Time.time - startTime) / overTime);
//		yield return new WaitForSeconds (0);
//
//		//			print ("Coroutine CameraZoomOut gamecontrol");
//
//		if (Camera.main.fieldOfView >= initZoom) {
//			StopCoroutine ("CameraZoomOut");
//		}
//	}
//	yield return new WaitForSeconds (0);
//
//}