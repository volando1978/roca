﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using SmartLocalization;

public class Autotype : MonoBehaviour
{
	
	public float letterPause = 0.2f;
	//		public AudioClip sound;
	List <string> description = new List<string> ();



	string message;


	void setStoneDescriptions ()
	{
		description.Add (LanguageManager.Instance.GetTextValue ("crystal"));
		description.Add (LanguageManager.Instance.GetTextValue ("crystal"));
		description.Add (LanguageManager.Instance.GetTextValue ("rubi"));
		description.Add (LanguageManager.Instance.GetTextValue ("opal"));
		description.Add (LanguageManager.Instance.GetTextValue ("saphire"));
		description.Add (LanguageManager.Instance.GetTextValue ("amethist"));
		description.Add (LanguageManager.Instance.GetTextValue ("agate"));
		description.Add (LanguageManager.Instance.GetTextValue ("diamond"));
		description.Add (LanguageManager.Instance.GetTextValue ("ice"));
		description.Add (LanguageManager.Instance.GetTextValue ("water"));
		description.Add (LanguageManager.Instance.GetTextValue ("potion"));
	}

	//TODO reset
	public void TriggerText ()
	{

//
//				GetComponent<Text> ().text = "";
//				setStoneDescriptions ();
//
//				switch (Random.Range (0, 5)) {
//				case 0:
//						message = description [globales.level] + "\n" + LanguageManager.Instance.GetTextValue ("turning");//player.GetComponent<PlayerMove> ().hSpeedCatalog [globales.level] + player.GetComponent<PlayerMove> ().intertiaPerStage [globales.level];
//						break;
//				case 1:
//						message = description [globales.level] + "\n" + LanguageManager.Instance.GetTextValue ("turning") + "\n" + LanguageManager.Instance.GetTextValue ("speed");//player.GetComponent<PlayerMove> ().hSpeedCatalog [globales.level] + player.GetComponent<PlayerMove> ().intertiaPerStage [globales.level];
//
//						break;
//				case 2:
//						message = description [globales.level] + "\n" + LanguageManager.Instance.GetTextValue ("speed");//player.GetComponent<PlayerMove> ().hSpeedCatalog [globales.level] + player.GetComponent<PlayerMove> ().intertiaPerStage [globales.level];
//
//						break;
//				case 4:
//						message = description [globales.level] + "\n" + LanguageManager.Instance.GetTextValue ("speed") + "\n" + LanguageManager.Instance.GetTextValue ("turning") + "\n" + LanguageManager.Instance.GetTextValue ("push");//player.GetComponent<PlayerMove> ().hSpeedCatalog [globales.level] + player.GetComponent<PlayerMove> ().intertiaPerStage [globales.level];
//
//						break;
//						
//				}
//				StartCoroutine (TypeText ());
//				SoundManager.playGem ();
	}

	IEnumerator TypeText ()
	{
		print (" message " + message);
		if (message != null) {
			foreach (char letter in message.ToCharArray()) {
				GetComponent<Text> ().text += letter;
				SoundManager.playTypeWriter ();
				yield return new WaitForSeconds (0);
			}      
		}
	}

}
