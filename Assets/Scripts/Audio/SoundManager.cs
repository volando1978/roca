using UnityEngine;
using System.Collections.Generic;


public class SoundManager : MonoBehaviour
{
	static int n;
	//		public GameObject MusicObj;
	//		public AudioSource music;

	[System.Serializable]
	private class soundLibrary
	{
		//bullets
		public  AudioClip startSnd;

		public  AudioClip checkOutSnd;
		public  AudioClip crashSnd;
		public  AudioClip smallCrashSnd;

		public  AudioClip menuButtonSnd;

		public  AudioClip fallSnd;
		public  AudioClip slidingSnd;
		public  AudioClip nendGameSnd;
		public  AudioClip nendGameSnd1;
		public  AudioClip nendGameSnd2;
		public  AudioClip coini;
		public AudioClip gem;
		public AudioClip typewriter;

	}


	[SerializeField]
	soundLibrary
		library;


	//bullets
	public static AudioClip startSnd;
	public static   AudioClip checkOutSnd;
	public static   AudioClip crashSnd;
	public static   AudioClip smallCrashSnd;
	public  static  AudioClip slidingSnd;
	public static   AudioClip menuButtonSnd;
	public  static  AudioClip fallSnd;
	public   static AudioClip endGameSnd;
	public   static AudioClip endGameSnd1;
	public   static AudioClip endGameSnd2;
	public  static AudioClip coini;
	public static AudioClip gemSnd;
	public static AudioClip typewriterSnd;



	public static AudioSource soundPlayer;
	public static float pan;
	bool isSliding = false;

	
	
	void Awake ()
	{
		startSnd = library.startSnd;
		checkOutSnd = library.checkOutSnd;
		crashSnd = library.crashSnd;
		smallCrashSnd = library.smallCrashSnd;
		slidingSnd = library.slidingSnd;
		menuButtonSnd = library.menuButtonSnd;
		fallSnd = library.fallSnd;
		endGameSnd = library.nendGameSnd;
		endGameSnd1 = library.nendGameSnd1;
		endGameSnd2 = library.nendGameSnd2;
		coini = library.coini;
		gemSnd = library.gem;
		typewriterSnd = library.typewriter;

		soundPlayer = GetComponent<AudioSource> ();
//				music = MusicObj.GetComponent<AudioSource> ();
	}

	public static void StopAllSounds ()
	{
		soundPlayer.Stop ();
	}

	public static void playStartClip ()
	{
		if (soundPlayer.clip != startSnd) {
//						soundPlayer.pitch = Random.Range (0.9f, 1.1f);
//				print ("BSSSSS");
//						soundPlayer.Stop ();
//
//						soundPlayer.volume = 1f;

			soundPlayer.PlayOneShot (startSnd);

		}
	}

	public static void playCheckOutClip ()
	{
		if (soundPlayer.clip != checkOutSnd) {
//						soundPlayer.Stop ();


			soundPlayer.PlayOneShot (checkOutSnd);
//						soundPlayer.volume = 1f;
		}
	}

	public static void playCoini ()
	{
//				soundPlayer.pitch = (float)(ProgController.nextProgression - globales.lastTotalLifeMeters) * 200;
		if (soundPlayer.clip != coini) {
//						soundPlayer.Stop ();

//						soundPlayer.volume = 1f;

			soundPlayer.PlayOneShot (coini);
		}
	}

	public static void playGem ()
	{
		soundPlayer.pitch = Random.Range (0.9f, 1.1f);
		if (soundPlayer.clip != gemSnd) {
//						soundPlayer.Stop ();
//
//						soundPlayer.volume = 1f;
			soundPlayer.PlayOneShot (gemSnd);
		}
	}

	public static void playTypeWriter ()
	{
		if (soundPlayer.clip != typewriterSnd) {
//						soundPlayer.Stop ();
//
//						soundPlayer.volume = 1f;

			soundPlayer.pitch = Random.Range (0.9f, 1.1f);
			soundPlayer.PlayOneShot (typewriterSnd);
		}
	}

	public static void playSmallCrashClip ()
	{

		if (soundPlayer.clip != smallCrashSnd) {
			soundPlayer.pitch = Random.Range (0.9f, 1.1f);
//						soundPlayer.Stop ();
//
//						soundPlayer.volume = 1f;

			soundPlayer.PlayOneShot (smallCrashSnd);
		}
	}

	public static void playCrashClip ()
	{
		soundPlayer.pitch = Random.Range (0.9f, 1.1f);
		if (soundPlayer.clip != crashSnd) {
//						soundPlayer.Stop ();
//
//						soundPlayer.volume = 1f;

			soundPlayer.PlayOneShot (crashSnd);
		}
	}

	public static void playButtontClip ()
	{
		if (soundPlayer) {
			soundPlayer.pitch = Random.Range (0.9f, 1.1f);
//						soundPlayer.Stop ();
//
//						soundPlayer.volume = 1f;

			soundPlayer.PlayOneShot (menuButtonSnd);
		}
	}

	public static void playFallingClip ()
	{
//				soundPlayer.pitch = Random.Range (0.9f, 1.1f);
		if (soundPlayer.clip != fallSnd) {
//						soundPlayer.Stop ();
//
//						soundPlayer.volume = 1f;

			soundPlayer.PlayOneShot (fallSnd);
		}
	}

	public static void playEndGameClip ()
	{
		int n = Random.Range (0, 2);
//				soundPlayer.Stop ();
		soundPlayer.pitch = Random.Range (0.9f, 1.1f);
//				soundPlayer.volume = 1f;
				
		switch (n) {
		case 0:
			soundPlayer.PlayOneShot (endGameSnd);
			break;
		case 1:
			soundPlayer.PlayOneShot (endGameSnd1);
			break;

		case 2:
			soundPlayer.PlayOneShot (endGameSnd2);
			break;
		}



	}
}


// /////////////////////////////////////////////////////////////////////////////////////////////////////////
//
// Audio Manager.
//
// This code is release under the MIT licence. It is provided as-is and without any warranty.
//
// Developed by Daniel Rodríguez (Seth Illgard) in April 2010
// http://www.silentkraken.com
//
// /////////////////////////////////////////////////////////////////////////////////////////////////////////
/*
using UnityEngine;
using System.Collections;

public class AudioManager : MonoBehaviour
{
	public AudioSource Play(AudioClip clip, Transform emitter)
	{
		return Play(clip, emitter, 1f, 1f);
	}
	
	public AudioSource Play(AudioClip clip, Transform emitter, float volume)
	{
		return Play(clip, emitter, volume, 1f);
	}
	
	/// <summary>
	/// Plays a sound by creating an empty game object with an AudioSource
	/// and attaching it to the given transform (so it moves with the transform). Destroys it after it finished playing.
	/// </summary>
	/// <param name="clip"></param>
	/// <param name="emitter"></param>
	/// <param name="volume"></param>
	/// <param name="pitch"></param>
	/// <returns></returns>
	public AudioSource Play(AudioClip clip, Transform emitter, float volume, float pitch)
	{
		//Create an empty game object
		GameObject go = new GameObject ("Audio: " +  clip.name);
		go.transform.position = emitter.position;
		go.transform.parent = emitter;
		
		//Create the source
		AudioSource source = go.AddComponent<AudioSource>();
		source.clip = clip;
		source.volume = volume;
		source.pitch = pitch;
		source.Play ();
		Destroy (go, clip.length);
		return source;
	}
	
	public AudioSource Play(AudioClip clip, Vector3 point)
	{
		return Play(clip, point, 1f, 1f);
	}
	
	public AudioSource Play(AudioClip clip, Vector3 point, float volume)
	{
		return Play(clip, point, volume, 1f);
	}
	
	/// <summary>
	/// Plays a sound at the given point in space by creating an empty game object with an AudioSource
	/// in that place and destroys it after it finished playing.
	/// </summary>
	/// <param name="clip"></param>
	/// <param name="point"></param>
	/// <param name="volume"></param>
	/// <param name="pitch"></param>
	/// <returns></returns>
	public AudioSource Play(AudioClip clip, Vector3 point, float volume, float pitch)
	{
		//Create an empty game object
		GameObject go = new GameObject("Audio: " + clip.name);
		go.transform.position = point;
		
		//Create the source
		AudioSource source = go.AddComponent<AudioSource>();
		source.clip = clip;
		source.volume = volume;
		source.pitch = pitch;
		source.Play();
		Destroy(go, clip.length);
		return source;
	}
}
*/