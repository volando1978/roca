using UnityEngine;
using System.Collections;

public class songsBatch : MonoBehaviour
{

		public ArrayList songs = new ArrayList ();
		public AudioClip switcherSound;


		public AudioClip track_1;
		public AudioClip track_2;
//		public AudioClip track_3;
//		public AudioClip track_4;
//		public AudioClip track_5;
//		public AudioClip track_6;
		public AudioClip track_7;

		AudioSource musicPlayer;

		static bool firstFade = true;

		// Use this for initialization
		void Awake ()
		{
				musicPlayer = GetComponent<AudioSource> ();
				//				switcherPlayer = GetComponent<AudioSource> ();
//				musicSwitchSound = switcherSound;

				songs.Add (track_1);
				songs.Add (track_2);
//				songs.Add (track_3);
//				songs.Add (track_4);
//				songs.Add (track_5);
//				songs.Add (track_6);
				songs.Add (track_7);	

				selectSong ();
				musicPlayer.volume = 0;
				StartCoroutine (musicFadeIn (0));


		}

		public IEnumerator  musicFadeIn (float fadeTime)
		{
				WaitForSeconds delay = new WaitForSeconds (fadeTime);
				while (musicPlayer.volume < 0.4f && firstFade) {
						musicPlayer.volume += 0.1f * Time.deltaTime;
						yield return delay;
//						firstFade = false;

				}
		}

		public void selectSong ()
		{

				musicPlayer.clip = songs [Random.Range (0, songs.Count)] as AudioClip;
				musicPlayer.pitch = Random.Range (0.8f, 1.2f);
				musicPlayer.Play ();
//				print ("select " + musicPlayer.volume);


		}

		public void switcherButton ()
		{
				
				musicPlayer.PlayOneShot (switcherSound);

		}
		void Update ()
		{
				if (!firstFade) {
						if (gameControl.currentState != gameControl.State.INGAME) {
								musicPlayer.volume = 0.3f;
						} else {
								musicPlayer.volume = 1;
			
						}
				}
		}
	
	
	
	
}
